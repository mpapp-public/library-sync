const { join } = require('path');
const { readFileSync, readdirSync } = require('fs');

const SCHEMA_DIR = 'schemas';

function getBuiltSchemas() {
  const schemasPath = join(__dirname, SCHEMA_DIR);
  const files = readdirSync(schemasPath, 'utf8');
  return files.map(filename => {
    const schemaPath = join(__dirname, SCHEMA_DIR, filename);
    const file = readFileSync(schemaPath, 'utf8');
    return JSON.parse(file);
  });
}

module.exports = {
  getBuiltSchemas
};

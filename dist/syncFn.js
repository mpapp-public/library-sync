function syncFn(doc, oldDoc) {
  /* istanbul ignore next */
  {
    var isArray = Array.isArray;
    var keyList = Object.keys;
    var hasProp = Object.prototype.hasOwnProperty;

    function equal(a, b) {
      if (a === b) return true;

      if (a && b && typeof a == 'object' && typeof b == 'object') {
        var arrA = isArray(a),
          arrB = isArray(b),
          i, length, key;

        if (arrA && arrB) {
          length = a.length;
          if (length != b.length) return false;
          for (i = length; i-- !== 0;)
            if (!equal(a[i], b[i])) return false;
          return true;
        }

        if (arrA != arrB) return false;

        var dateA = a instanceof Date,
          dateB = b instanceof Date;
        if (dateA != dateB) return false;
        if (dateA && dateB) return a.getTime() == b.getTime();

        var regexpA = a instanceof RegExp,
          regexpB = b instanceof RegExp;
        if (regexpA != regexpB) return false;
        if (regexpA && regexpB) return a.toString() == b.toString();

        var keys = keyList(a);
        length = keys.length;

        if (length !== keyList(b).length)
          return false;

        for (i = length; i-- !== 0;)
          if (!hasProp.call(b, keys[i])) return false;

        for (i = length; i-- !== 0;) {
          key = keys[i];
          if (!equal(a[key], b[key])) return false;
        }

        return true;
      }

      return a !== a && b !== b;
    }


    function MPAffiliation(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?Manuscript');
      var pattern2 = new RegExp('^(MP)?Project');
      var pattern3 = new RegExp('^(MP)?Library');
      var pattern4 = new RegExp('^MPKeyword:[0-9a-zA-Z\\-]+');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      };
      var refVal3 = {
        "type": "string",
        "pattern": "^MPKeyword:[0-9a-zA-Z\\-]+"
      }; /*# sourceURL=MPAffiliation.json */
      var vErrors = null;
      var errors = 0;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || MPAffiliation.schema.properties.hasOwnProperty(key0));
            if (isAdditional0) {
              valid1 = false;
              MPAffiliation.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data.manuscript;
            if (data1 === undefined) {
              valid1 = false;
              MPAffiliation.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: 'manuscript'
                },
                message: 'should have required property \'manuscript\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPAffiliation.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '.manuscript',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPAffiliation.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '.manuscript',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPAffiliation.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.manuscript',
                      schemaPath: '#/properties/manuscript/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?Manuscript'
                      },
                      message: 'should match pattern "^(MP)?Manuscript"'
                    }];
                    return false;
                  }
                } else {
                  MPAffiliation.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.manuscript',
                    schemaPath: '#/properties/manuscript/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              var data1 = data.container_id;
              if (data1 === undefined) {
                valid1 = false;
                MPAffiliation.errors = [{
                  keyword: 'required',
                  dataPath: (dataPath || '') + "",
                  schemaPath: '#/required',
                  params: {
                    missingProperty: 'container_id'
                  },
                  message: 'should have required property \'container_id\''
                }];
                return false;
              } else {
                var errs_1 = errors;
                var errs_2 = errors;
                var errs_3 = errors;
                if (typeof data1 === "string") {
                  if (!pattern0.test(data1)) {
                    MPAffiliation.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: 'strings.json#/definitions/_id/pattern',
                      params: {
                        pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                      },
                      message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                    }];
                    return false;
                  }
                } else {
                  MPAffiliation.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.container_id',
                    schemaPath: 'strings.json#/definitions/_id/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid3 = errors === errs_3;
                var valid2 = errors === errs_2;
                if (valid2) {
                  var errs_2 = errors;
                  var errs__2 = errors;
                  var valid2 = false;
                  var errs_3 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern2.test(data1)) {
                      var err = {
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '.container_id',
                        schemaPath: '#/properties/container_id/allOf/1/anyOf/0/pattern',
                        params: {
                          pattern: '^(MP)?Project'
                        },
                        message: 'should match pattern "^(MP)?Project"'
                      };
                      if (vErrors === null) vErrors = [err];
                      else vErrors.push(err);
                      errors++;
                    }
                  } else {
                    var err = {
                      keyword: 'type',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: '#/properties/container_id/allOf/1/anyOf/0/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    };
                    if (vErrors === null) vErrors = [err];
                    else vErrors.push(err);
                    errors++;
                  }
                  var valid3 = errors === errs_3;
                  valid2 = valid2 || valid3;
                  if (!valid2) {
                    var errs_3 = errors;
                    if (typeof data1 === "string") {
                      if (!pattern3.test(data1)) {
                        var err = {
                          keyword: 'pattern',
                          dataPath: (dataPath || '') + '.container_id',
                          schemaPath: '#/properties/container_id/allOf/1/anyOf/1/pattern',
                          params: {
                            pattern: '^(MP)?Library'
                          },
                          message: 'should match pattern "^(MP)?Library"'
                        };
                        if (vErrors === null) vErrors = [err];
                        else vErrors.push(err);
                        errors++;
                      }
                    } else {
                      var err = {
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.container_id',
                        schemaPath: '#/properties/container_id/allOf/1/anyOf/1/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      };
                      if (vErrors === null) vErrors = [err];
                      else vErrors.push(err);
                      errors++;
                    }
                    var valid3 = errors === errs_3;
                    valid2 = valid2 || valid3;
                  }
                  if (!valid2) {
                    var err = {
                      keyword: 'anyOf',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: '#/properties/container_id/allOf/1/anyOf',
                      params: {},
                      message: 'should match some schema in anyOf'
                    };
                    if (vErrors === null) vErrors = [err];
                    else vErrors.push(err);
                    errors++;
                    MPAffiliation.errors = vErrors;
                    return false;
                  } else {
                    errors = errs__2;
                    if (vErrors !== null) {
                      if (errs__2) vErrors.length = errs__2;
                      else vErrors = null;
                    }
                  }
                  var valid2 = errors === errs_2;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data._id;
                if (data1 === undefined) {
                  valid1 = false;
                  MPAffiliation.errors = [{
                    keyword: 'required',
                    dataPath: (dataPath || '') + "",
                    schemaPath: '#/required',
                    params: {
                      missingProperty: '_id'
                    },
                    message: 'should have required property \'_id\''
                  }];
                  return false;
                } else {
                  var errs_1 = errors;
                  var errs_2 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern0.test(data1)) {
                      MPAffiliation.errors = [{
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '._id',
                        schemaPath: 'strings.json#/definitions/_id/pattern',
                        params: {
                          pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                        },
                        message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                      }];
                      return false;
                    }
                  } else {
                    MPAffiliation.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: 'strings.json#/definitions/_id/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    }];
                    return false;
                  }
                  var valid2 = errors === errs_2;
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  if (data._rev === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    if (typeof data._rev !== "string") {
                      MPAffiliation.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '._rev',
                        schemaPath: '#/properties/_rev/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data._revisions;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                        var errs__1 = errors;
                        var valid2 = true;
                        var data2 = data1.start;
                        if (data2 === undefined) {
                          valid2 = true;
                        } else {
                          var errs_2 = errors;
                          if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                            MPAffiliation.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '._revisions.start',
                              schemaPath: '#/properties/_revisions/properties/start/type',
                              params: {
                                type: 'integer'
                              },
                              message: 'should be integer'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                        }
                        if (valid2) {
                          var data2 = data1.ids;
                          if (data2 === undefined) {
                            valid2 = true;
                          } else {
                            var errs_2 = errors;
                            if (Array.isArray(data2)) {
                              var errs__2 = errors;
                              var valid2;
                              for (var i2 = 0; i2 < data2.length; i2++) {
                                var errs_3 = errors;
                                if (typeof data2[i2] !== "string") {
                                  MPAffiliation.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                    schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid3 = errors === errs_3;
                                if (!valid3) break;
                              }
                            } else {
                              MPAffiliation.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids',
                                schemaPath: '#/properties/_revisions/properties/ids/type',
                                params: {
                                  type: 'array'
                                },
                                message: 'should be array'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                          }
                        }
                      } else {
                        MPAffiliation.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions',
                          schemaPath: '#/properties/_revisions/type',
                          params: {
                            type: 'object'
                          },
                          message: 'should be object'
                        }];
                        return false;
                      }
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      if (data.sessionID === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        if (typeof data.sessionID !== "string") {
                          MPAffiliation.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.sessionID',
                            schemaPath: '#/properties/sessionID/type',
                            params: {
                              type: 'string'
                            },
                            message: 'should be string'
                          }];
                          return false;
                        }
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data.createdAt;
                        if (data1 === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          var errs_2 = errors;
                          if (typeof data1 === "number") {
                            if (data1 > 2000000000 || data1 !== data1) {
                              MPAffiliation.errors = [{
                                keyword: 'maximum',
                                dataPath: (dataPath || '') + '.createdAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                params: {
                                  comparison: '<=',
                                  limit: 2000000000,
                                  exclusive: false
                                },
                                message: 'should be <= 2000000000'
                              }];
                              return false;
                            } else {
                              if (data1 < 0 || data1 !== data1) {
                                MPAffiliation.errors = [{
                                  keyword: 'minimum',
                                  dataPath: (dataPath || '') + '.createdAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                  params: {
                                    comparison: '>=',
                                    limit: 0,
                                    exclusive: false
                                  },
                                  message: 'should be >= 0'
                                }];
                                return false;
                              }
                            }
                          } else {
                            MPAffiliation.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.createdAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/type',
                              params: {
                                type: 'number'
                              },
                              message: 'should be number'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          var data1 = data.updatedAt;
                          if (data1 === undefined) {
                            valid1 = true;
                          } else {
                            var errs_1 = errors;
                            var errs_2 = errors;
                            if (typeof data1 === "number") {
                              if (data1 > 2000000000 || data1 !== data1) {
                                MPAffiliation.errors = [{
                                  keyword: 'maximum',
                                  dataPath: (dataPath || '') + '.updatedAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                  params: {
                                    comparison: '<=',
                                    limit: 2000000000,
                                    exclusive: false
                                  },
                                  message: 'should be <= 2000000000'
                                }];
                                return false;
                              } else {
                                if (data1 < 0 || data1 !== data1) {
                                  MPAffiliation.errors = [{
                                    keyword: 'minimum',
                                    dataPath: (dataPath || '') + '.updatedAt',
                                    schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                    params: {
                                      comparison: '>=',
                                      limit: 0,
                                      exclusive: false
                                    },
                                    message: 'should be >= 0'
                                  }];
                                  return false;
                                }
                              }
                            } else {
                              MPAffiliation.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.updatedAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/type',
                                params: {
                                  type: 'number'
                                },
                                message: 'should be number'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            var data1 = data.keywordIDs;
                            if (data1 === undefined) {
                              valid1 = true;
                            } else {
                              var errs_1 = errors;
                              if (Array.isArray(data1)) {
                                var errs__1 = errors;
                                var valid1;
                                for (var i1 = 0; i1 < data1.length; i1++) {
                                  var data2 = data1[i1];
                                  var errs_2 = errors;
                                  var errs_3 = errors;
                                  if (typeof data2 === "string") {
                                    if (!pattern4.test(data2)) {
                                      MPAffiliation.errors = [{
                                        keyword: 'pattern',
                                        dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                        schemaPath: 'strings.json#/definitions/keywordId/pattern',
                                        params: {
                                          pattern: '^MPKeyword:[0-9a-zA-Z\\-]+'
                                        },
                                        message: 'should match pattern "^MPKeyword:[0-9a-zA-Z\\-]+"'
                                      }];
                                      return false;
                                    }
                                  } else {
                                    MPAffiliation.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                      schemaPath: 'strings.json#/definitions/keywordId/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid3 = errors === errs_3;
                                  var valid2 = errors === errs_2;
                                  if (!valid2) break;
                                }
                              } else {
                                MPAffiliation.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.keywordIDs',
                                  schemaPath: '#/properties/keywordIDs/type',
                                  params: {
                                    type: 'array'
                                  },
                                  message: 'should be array'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              if (data.addressLine1 === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if (typeof data.addressLine1 !== "string") {
                                  MPAffiliation.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.addressLine1',
                                    schemaPath: '#/properties/addressLine1/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.addressLine2 === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.addressLine2 !== "string") {
                                    MPAffiliation.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.addressLine2',
                                      schemaPath: '#/properties/addressLine2/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                                if (valid1) {
                                  if (data.addressLine3 === undefined) {
                                    valid1 = true;
                                  } else {
                                    var errs_1 = errors;
                                    if (typeof data.addressLine3 !== "string") {
                                      MPAffiliation.errors = [{
                                        keyword: 'type',
                                        dataPath: (dataPath || '') + '.addressLine3',
                                        schemaPath: '#/properties/addressLine3/type',
                                        params: {
                                          type: 'string'
                                        },
                                        message: 'should be string'
                                      }];
                                      return false;
                                    }
                                    var valid1 = errors === errs_1;
                                  }
                                  if (valid1) {
                                    if (data.city === undefined) {
                                      valid1 = true;
                                    } else {
                                      var errs_1 = errors;
                                      if (typeof data.city !== "string") {
                                        MPAffiliation.errors = [{
                                          keyword: 'type',
                                          dataPath: (dataPath || '') + '.city',
                                          schemaPath: '#/properties/city/type',
                                          params: {
                                            type: 'string'
                                          },
                                          message: 'should be string'
                                        }];
                                        return false;
                                      }
                                      var valid1 = errors === errs_1;
                                    }
                                    if (valid1) {
                                      if (data.country === undefined) {
                                        valid1 = true;
                                      } else {
                                        var errs_1 = errors;
                                        if (typeof data.country !== "string") {
                                          MPAffiliation.errors = [{
                                            keyword: 'type',
                                            dataPath: (dataPath || '') + '.country',
                                            schemaPath: '#/properties/country/type',
                                            params: {
                                              type: 'string'
                                            },
                                            message: 'should be string'
                                          }];
                                          return false;
                                        }
                                        var valid1 = errors === errs_1;
                                      }
                                      if (valid1) {
                                        if (data.county === undefined) {
                                          valid1 = true;
                                        } else {
                                          var errs_1 = errors;
                                          if (typeof data.county !== "string") {
                                            MPAffiliation.errors = [{
                                              keyword: 'type',
                                              dataPath: (dataPath || '') + '.county',
                                              schemaPath: '#/properties/county/type',
                                              params: {
                                                type: 'string'
                                              },
                                              message: 'should be string'
                                            }];
                                            return false;
                                          }
                                          var valid1 = errors === errs_1;
                                        }
                                        if (valid1) {
                                          if (data.department === undefined) {
                                            valid1 = true;
                                          } else {
                                            var errs_1 = errors;
                                            if (typeof data.department !== "string") {
                                              MPAffiliation.errors = [{
                                                keyword: 'type',
                                                dataPath: (dataPath || '') + '.department',
                                                schemaPath: '#/properties/department/type',
                                                params: {
                                                  type: 'string'
                                                },
                                                message: 'should be string'
                                              }];
                                              return false;
                                            }
                                            var valid1 = errors === errs_1;
                                          }
                                          if (valid1) {
                                            if (data.institution === undefined) {
                                              valid1 = true;
                                            } else {
                                              var errs_1 = errors;
                                              if (typeof data.institution !== "string") {
                                                MPAffiliation.errors = [{
                                                  keyword: 'type',
                                                  dataPath: (dataPath || '') + '.institution',
                                                  schemaPath: '#/properties/institution/type',
                                                  params: {
                                                    type: 'string'
                                                  },
                                                  message: 'should be string'
                                                }];
                                                return false;
                                              }
                                              var valid1 = errors === errs_1;
                                            }
                                            if (valid1) {
                                              var data1 = data.objectType;
                                              if (data1 === undefined) {
                                                valid1 = false;
                                                MPAffiliation.errors = [{
                                                  keyword: 'required',
                                                  dataPath: (dataPath || '') + "",
                                                  schemaPath: '#/required',
                                                  params: {
                                                    missingProperty: 'objectType'
                                                  },
                                                  message: 'should have required property \'objectType\''
                                                }];
                                                return false;
                                              } else {
                                                var errs_1 = errors;
                                                if (typeof data1 !== "string") {
                                                  MPAffiliation.errors = [{
                                                    keyword: 'type',
                                                    dataPath: (dataPath || '') + '.objectType',
                                                    schemaPath: '#/properties/objectType/type',
                                                    params: {
                                                      type: 'string'
                                                    },
                                                    message: 'should be string'
                                                  }];
                                                  return false;
                                                }
                                                var schema1 = MPAffiliation.schema.properties.objectType.enum;
                                                var valid1;
                                                valid1 = false;
                                                for (var i1 = 0; i1 < schema1.length; i1++)
                                                  if (equal(data1, schema1[i1])) {
                                                    valid1 = true;
                                                    break;
                                                  }
                                                if (!valid1) {
                                                  MPAffiliation.errors = [{
                                                    keyword: 'enum',
                                                    dataPath: (dataPath || '') + '.objectType',
                                                    schemaPath: '#/properties/objectType/enum',
                                                    params: {
                                                      allowedValues: schema1
                                                    },
                                                    message: 'should be equal to one of the allowed values'
                                                  }];
                                                  return false;
                                                }
                                                var valid1 = errors === errs_1;
                                              }
                                              if (valid1) {
                                                if (data.postCode === undefined) {
                                                  valid1 = true;
                                                } else {
                                                  var errs_1 = errors;
                                                  if (typeof data.postCode !== "string") {
                                                    MPAffiliation.errors = [{
                                                      keyword: 'type',
                                                      dataPath: (dataPath || '') + '.postCode',
                                                      schemaPath: '#/properties/postCode/type',
                                                      params: {
                                                        type: 'string'
                                                      },
                                                      message: 'should be string'
                                                    }];
                                                    return false;
                                                  }
                                                  var valid1 = errors === errs_1;
                                                }
                                                if (valid1) {
                                                  if (data.priority === undefined) {
                                                    valid1 = true;
                                                  } else {
                                                    var errs_1 = errors;
                                                    if (typeof data.priority !== "number") {
                                                      MPAffiliation.errors = [{
                                                        keyword: 'type',
                                                        dataPath: (dataPath || '') + '.priority',
                                                        schemaPath: '#/properties/priority/type',
                                                        params: {
                                                          type: 'number'
                                                        },
                                                        message: 'should be number'
                                                      }];
                                                      return false;
                                                    }
                                                    var valid1 = errors === errs_1;
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPAffiliation.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPAffiliation.errors = vErrors;
      return errors === 0;
    };
    MPAffiliation.schema = {
      "$id": "MPAffiliation.json",
      "additionalProperties": false,
      "properties": {
        "manuscript": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?Manuscript"
          }]
        },
        "container_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "anyOf": [{
              "type": "string",
              "pattern": "^(MP)?Project"
            }, {
              "type": "string",
              "pattern": "^(MP)?Library"
            }]
          }]
        },
        "_id": {
          "$ref": "strings.json#/definitions/_id"
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "sessionID": {
          "type": "string"
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "keywordIDs": {
          "type": "array",
          "items": {
            "$ref": "strings.json#/definitions/keywordId"
          }
        },
        "addressLine1": {
          "type": "string"
        },
        "addressLine2": {
          "type": "string"
        },
        "addressLine3": {
          "type": "string"
        },
        "city": {
          "type": "string"
        },
        "country": {
          "type": "string"
        },
        "county": {
          "type": "string"
        },
        "department": {
          "type": "string"
        },
        "institution": {
          "type": "string"
        },
        "objectType": {
          "type": "string",
          "enum": ["Affiliation", "MPAffiliation"]
        },
        "postCode": {
          "type": "string"
        },
        "priority": {
          "type": "number"
        }
      },
      "required": ["manuscript", "container_id", "_id", "objectType"],
      "title": "Affiliation",
      "type": "object"
    };
    MPAffiliation.errors = null;

    function MPBibliographicDate(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?BibliographicDate');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      }; /*# sourceURL=MPBibliographicDate.json */
      var vErrors = null;
      var errors = 0;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || MPBibliographicDate.schema.properties.hasOwnProperty(key0));
            if (isAdditional0) {
              valid1 = false;
              MPBibliographicDate.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data._id;
            if (data1 === undefined) {
              valid1 = false;
              MPBibliographicDate.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: '_id'
                },
                message: 'should have required property \'_id\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPBibliographicDate.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPBibliographicDate.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '._id',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPBibliographicDate.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: '#/properties/_id/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?BibliographicDate'
                      },
                      message: 'should match pattern "^(MP)?BibliographicDate"'
                    }];
                    return false;
                  }
                } else {
                  MPBibliographicDate.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: '#/properties/_id/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              if (data._rev === undefined) {
                valid1 = true;
              } else {
                var errs_1 = errors;
                if (typeof data._rev !== "string") {
                  MPBibliographicDate.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._rev',
                    schemaPath: '#/properties/_rev/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data._revisions;
                if (data1 === undefined) {
                  valid1 = true;
                } else {
                  var errs_1 = errors;
                  if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                    var errs__1 = errors;
                    var valid2 = true;
                    var data2 = data1.start;
                    if (data2 === undefined) {
                      valid2 = true;
                    } else {
                      var errs_2 = errors;
                      if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                        MPBibliographicDate.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions.start',
                          schemaPath: '#/properties/_revisions/properties/start/type',
                          params: {
                            type: 'integer'
                          },
                          message: 'should be integer'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                    }
                    if (valid2) {
                      var data2 = data1.ids;
                      if (data2 === undefined) {
                        valid2 = true;
                      } else {
                        var errs_2 = errors;
                        if (Array.isArray(data2)) {
                          var errs__2 = errors;
                          var valid2;
                          for (var i2 = 0; i2 < data2.length; i2++) {
                            var errs_3 = errors;
                            if (typeof data2[i2] !== "string") {
                              MPBibliographicDate.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var valid3 = errors === errs_3;
                            if (!valid3) break;
                          }
                        } else {
                          MPBibliographicDate.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '._revisions.ids',
                            schemaPath: '#/properties/_revisions/properties/ids/type',
                            params: {
                              type: 'array'
                            },
                            message: 'should be array'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                      }
                    }
                  } else {
                    MPBibliographicDate.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._revisions',
                      schemaPath: '#/properties/_revisions/type',
                      params: {
                        type: 'object'
                      },
                      message: 'should be object'
                    }];
                    return false;
                  }
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  var data1 = data.createdAt;
                  if (data1 === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    var errs_2 = errors;
                    if (typeof data1 === "number") {
                      if (data1 > 2000000000 || data1 !== data1) {
                        MPBibliographicDate.errors = [{
                          keyword: 'maximum',
                          dataPath: (dataPath || '') + '.createdAt',
                          schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                          params: {
                            comparison: '<=',
                            limit: 2000000000,
                            exclusive: false
                          },
                          message: 'should be <= 2000000000'
                        }];
                        return false;
                      } else {
                        if (data1 < 0 || data1 !== data1) {
                          MPBibliographicDate.errors = [{
                            keyword: 'minimum',
                            dataPath: (dataPath || '') + '.createdAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                            params: {
                              comparison: '>=',
                              limit: 0,
                              exclusive: false
                            },
                            message: 'should be >= 0'
                          }];
                          return false;
                        }
                      }
                    } else {
                      MPBibliographicDate.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.createdAt',
                        schemaPath: 'numbers.json#/definitions/timestamp/type',
                        params: {
                          type: 'number'
                        },
                        message: 'should be number'
                      }];
                      return false;
                    }
                    var valid2 = errors === errs_2;
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data.updatedAt;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      var errs_2 = errors;
                      if (typeof data1 === "number") {
                        if (data1 > 2000000000 || data1 !== data1) {
                          MPBibliographicDate.errors = [{
                            keyword: 'maximum',
                            dataPath: (dataPath || '') + '.updatedAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                            params: {
                              comparison: '<=',
                              limit: 2000000000,
                              exclusive: false
                            },
                            message: 'should be <= 2000000000'
                          }];
                          return false;
                        } else {
                          if (data1 < 0 || data1 !== data1) {
                            MPBibliographicDate.errors = [{
                              keyword: 'minimum',
                              dataPath: (dataPath || '') + '.updatedAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                              params: {
                                comparison: '>=',
                                limit: 0,
                                exclusive: false
                              },
                              message: 'should be >= 0'
                            }];
                            return false;
                          }
                        }
                      } else {
                        MPBibliographicDate.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '.updatedAt',
                          schemaPath: 'numbers.json#/definitions/timestamp/type',
                          params: {
                            type: 'number'
                          },
                          message: 'should be number'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      var data1 = data.objectType;
                      if (data1 === undefined) {
                        valid1 = false;
                        MPBibliographicDate.errors = [{
                          keyword: 'required',
                          dataPath: (dataPath || '') + "",
                          schemaPath: '#/required',
                          params: {
                            missingProperty: 'objectType'
                          },
                          message: 'should have required property \'objectType\''
                        }];
                        return false;
                      } else {
                        var errs_1 = errors;
                        if (typeof data1 !== "string") {
                          MPBibliographicDate.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.objectType',
                            schemaPath: '#/properties/objectType/type',
                            params: {
                              type: 'string'
                            },
                            message: 'should be string'
                          }];
                          return false;
                        }
                        var schema1 = MPBibliographicDate.schema.properties.objectType.enum;
                        var valid1;
                        valid1 = false;
                        for (var i1 = 0; i1 < schema1.length; i1++)
                          if (equal(data1, schema1[i1])) {
                            valid1 = true;
                            break;
                          }
                        if (!valid1) {
                          MPBibliographicDate.errors = [{
                            keyword: 'enum',
                            dataPath: (dataPath || '') + '.objectType',
                            schemaPath: '#/properties/objectType/enum',
                            params: {
                              allowedValues: schema1
                            },
                            message: 'should be equal to one of the allowed values'
                          }];
                          return false;
                        }
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data['date-parts'];
                        if (data1 === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          if (Array.isArray(data1)) {
                            if (data1.length > 2) {
                              MPBibliographicDate.errors = [{
                                keyword: 'maxItems',
                                dataPath: (dataPath || '') + '[\'date-parts\']',
                                schemaPath: '#/properties/date-parts/maxItems',
                                params: {
                                  limit: 2
                                },
                                message: 'should NOT have more than 2 items'
                              }];
                              return false;
                            } else {
                              var errs__1 = errors;
                              var valid1;
                              for (var i1 = 0; i1 < data1.length; i1++) {
                                var data2 = data1[i1];
                                var errs_2 = errors;
                                if (Array.isArray(data2)) {
                                  if (data2.length > 3) {
                                    MPBibliographicDate.errors = [{
                                      keyword: 'maxItems',
                                      dataPath: (dataPath || '') + '[\'date-parts\'][' + i1 + ']',
                                      schemaPath: '#/properties/date-parts/items/maxItems',
                                      params: {
                                        limit: 3
                                      },
                                      message: 'should NOT have more than 3 items'
                                    }];
                                    return false;
                                  } else {
                                    var errs__2 = errors;
                                    var valid2;
                                    for (var i2 = 0; i2 < data2.length; i2++) {
                                      var data3 = data2[i2];
                                      var errs_3 = errors;
                                      if (typeof data3 !== "string" && typeof data3 !== "number") {
                                        MPBibliographicDate.errors = [{
                                          keyword: 'type',
                                          dataPath: (dataPath || '') + '[\'date-parts\'][' + i1 + '][' + i2 + ']',
                                          schemaPath: '#/properties/date-parts/items/items/type',
                                          params: {
                                            type: 'string,number'
                                          },
                                          message: 'should be string,number'
                                        }];
                                        return false;
                                      }
                                      var valid3 = errors === errs_3;
                                      if (!valid3) break;
                                    }
                                  }
                                } else {
                                  MPBibliographicDate.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '[\'date-parts\'][' + i1 + ']',
                                    schemaPath: '#/properties/date-parts/items/type',
                                    params: {
                                      type: 'array'
                                    },
                                    message: 'should be array'
                                  }];
                                  return false;
                                }
                                var valid2 = errors === errs_2;
                                if (!valid2) break;
                              }
                            }
                          } else {
                            MPBibliographicDate.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '[\'date-parts\']',
                              schemaPath: '#/properties/date-parts/type',
                              params: {
                                type: 'array'
                              },
                              message: 'should be array'
                            }];
                            return false;
                          }
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          var data1 = data.season;
                          if (data1 === undefined) {
                            valid1 = true;
                          } else {
                            var errs_1 = errors;
                            if (typeof data1 === "number") {
                              if (data1 > 4 || data1 !== data1) {
                                MPBibliographicDate.errors = [{
                                  keyword: 'maximum',
                                  dataPath: (dataPath || '') + '.season',
                                  schemaPath: '#/properties/season/maximum',
                                  params: {
                                    comparison: '<=',
                                    limit: 4,
                                    exclusive: false
                                  },
                                  message: 'should be <= 4'
                                }];
                                return false;
                              } else {
                                if (data1 < 0 || data1 !== data1) {
                                  MPBibliographicDate.errors = [{
                                    keyword: 'minimum',
                                    dataPath: (dataPath || '') + '.season',
                                    schemaPath: '#/properties/season/minimum',
                                    params: {
                                      comparison: '>=',
                                      limit: 0,
                                      exclusive: false
                                    },
                                    message: 'should be >= 0'
                                  }];
                                  return false;
                                }
                              }
                            } else {
                              MPBibliographicDate.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.season',
                                schemaPath: '#/properties/season/type',
                                params: {
                                  type: 'number'
                                },
                                message: 'should be number'
                              }];
                              return false;
                            }
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            if (data.circa === undefined) {
                              valid1 = true;
                            } else {
                              var errs_1 = errors;
                              if (typeof data.circa !== "boolean") {
                                MPBibliographicDate.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.circa',
                                  schemaPath: '#/properties/circa/type',
                                  params: {
                                    type: 'boolean'
                                  },
                                  message: 'should be boolean'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              if (data.literal === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if (typeof data.literal !== "string") {
                                  MPBibliographicDate.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.literal',
                                    schemaPath: '#/properties/literal/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.raw === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.raw !== "string") {
                                    MPBibliographicDate.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.raw',
                                      schemaPath: '#/properties/raw/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPBibliographicDate.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPBibliographicDate.errors = vErrors;
      return errors === 0;
    };
    MPBibliographicDate.schema = {
      "$id": "MPBibliographicDate.json",
      "additionalProperties": false,
      "properties": {
        "_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?BibliographicDate"
          }]
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "objectType": {
          "type": "string",
          "enum": ["BibliographicDate", "MPBibliographicDate"]
        },
        "date-parts": {
          "type": "array",
          "items": {
            "type": "array",
            "items": {
              "type": ["string", "number"]
            },
            "maxItems": 3
          },
          "maxItems": 2
        },
        "season": {
          "type": "number",
          "minimum": 0,
          "maximum": 4
        },
        "circa": {
          "type": "boolean"
        },
        "literal": {
          "type": "string"
        },
        "raw": {
          "type": "string"
        }
      },
      "required": ["_id", "objectType"],
      "type": "object"
    };
    MPBibliographicDate.errors = null;

    function MPBibliographicName(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?BibliographicName');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      }; /*# sourceURL=MPBibliographicName.json */
      var vErrors = null;
      var errors = 0;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          var data1 = data._id;
          if (data1 === undefined) {
            valid1 = false;
            MPBibliographicName.errors = [{
              keyword: 'required',
              dataPath: (dataPath || '') + "",
              schemaPath: '#/required',
              params: {
                missingProperty: '_id'
              },
              message: 'should have required property \'_id\''
            }];
            return false;
          } else {
            var errs_1 = errors;
            var errs_2 = errors;
            var errs_3 = errors;
            if (typeof data1 === "string") {
              if (!pattern0.test(data1)) {
                MPBibliographicName.errors = [{
                  keyword: 'pattern',
                  dataPath: (dataPath || '') + '._id',
                  schemaPath: 'strings.json#/definitions/_id/pattern',
                  params: {
                    pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                  },
                  message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                }];
                return false;
              }
            } else {
              MPBibliographicName.errors = [{
                keyword: 'type',
                dataPath: (dataPath || '') + '._id',
                schemaPath: 'strings.json#/definitions/_id/type',
                params: {
                  type: 'string'
                },
                message: 'should be string'
              }];
              return false;
            }
            var valid3 = errors === errs_3;
            var valid2 = errors === errs_2;
            if (valid2) {
              var errs_2 = errors;
              if (typeof data1 === "string") {
                if (!pattern1.test(data1)) {
                  MPBibliographicName.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: '#/properties/_id/allOf/1/pattern',
                    params: {
                      pattern: '^(MP)?BibliographicName'
                    },
                    message: 'should match pattern "^(MP)?BibliographicName"'
                  }];
                  return false;
                }
              } else {
                MPBibliographicName.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '._id',
                  schemaPath: '#/properties/_id/allOf/1/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid2 = errors === errs_2;
            }
            var valid1 = errors === errs_1;
          }
          if (valid1) {
            if (data._rev === undefined) {
              valid1 = true;
            } else {
              var errs_1 = errors;
              if (typeof data._rev !== "string") {
                MPBibliographicName.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '._rev',
                  schemaPath: '#/properties/_rev/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              var data1 = data._revisions;
              if (data1 === undefined) {
                valid1 = true;
              } else {
                var errs_1 = errors;
                if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                  var errs__1 = errors;
                  var valid2 = true;
                  var data2 = data1.start;
                  if (data2 === undefined) {
                    valid2 = true;
                  } else {
                    var errs_2 = errors;
                    if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                      MPBibliographicName.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '._revisions.start',
                        schemaPath: '#/properties/_revisions/properties/start/type',
                        params: {
                          type: 'integer'
                        },
                        message: 'should be integer'
                      }];
                      return false;
                    }
                    var valid2 = errors === errs_2;
                  }
                  if (valid2) {
                    var data2 = data1.ids;
                    if (data2 === undefined) {
                      valid2 = true;
                    } else {
                      var errs_2 = errors;
                      if (Array.isArray(data2)) {
                        var errs__2 = errors;
                        var valid2;
                        for (var i2 = 0; i2 < data2.length; i2++) {
                          var errs_3 = errors;
                          if (typeof data2[i2] !== "string") {
                            MPBibliographicName.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                              schemaPath: '#/properties/_revisions/properties/ids/items/type',
                              params: {
                                type: 'string'
                              },
                              message: 'should be string'
                            }];
                            return false;
                          }
                          var valid3 = errors === errs_3;
                          if (!valid3) break;
                        }
                      } else {
                        MPBibliographicName.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions.ids',
                          schemaPath: '#/properties/_revisions/properties/ids/type',
                          params: {
                            type: 'array'
                          },
                          message: 'should be array'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                    }
                  }
                } else {
                  MPBibliographicName.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._revisions',
                    schemaPath: '#/properties/_revisions/type',
                    params: {
                      type: 'object'
                    },
                    message: 'should be object'
                  }];
                  return false;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data.createdAt;
                if (data1 === undefined) {
                  valid1 = true;
                } else {
                  var errs_1 = errors;
                  var errs_2 = errors;
                  if (typeof data1 === "number") {
                    if (data1 > 2000000000 || data1 !== data1) {
                      MPBibliographicName.errors = [{
                        keyword: 'maximum',
                        dataPath: (dataPath || '') + '.createdAt',
                        schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                        params: {
                          comparison: '<=',
                          limit: 2000000000,
                          exclusive: false
                        },
                        message: 'should be <= 2000000000'
                      }];
                      return false;
                    } else {
                      if (data1 < 0 || data1 !== data1) {
                        MPBibliographicName.errors = [{
                          keyword: 'minimum',
                          dataPath: (dataPath || '') + '.createdAt',
                          schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                          params: {
                            comparison: '>=',
                            limit: 0,
                            exclusive: false
                          },
                          message: 'should be >= 0'
                        }];
                        return false;
                      }
                    }
                  } else {
                    MPBibliographicName.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '.createdAt',
                      schemaPath: 'numbers.json#/definitions/timestamp/type',
                      params: {
                        type: 'number'
                      },
                      message: 'should be number'
                    }];
                    return false;
                  }
                  var valid2 = errors === errs_2;
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  var data1 = data.updatedAt;
                  if (data1 === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    var errs_2 = errors;
                    if (typeof data1 === "number") {
                      if (data1 > 2000000000 || data1 !== data1) {
                        MPBibliographicName.errors = [{
                          keyword: 'maximum',
                          dataPath: (dataPath || '') + '.updatedAt',
                          schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                          params: {
                            comparison: '<=',
                            limit: 2000000000,
                            exclusive: false
                          },
                          message: 'should be <= 2000000000'
                        }];
                        return false;
                      } else {
                        if (data1 < 0 || data1 !== data1) {
                          MPBibliographicName.errors = [{
                            keyword: 'minimum',
                            dataPath: (dataPath || '') + '.updatedAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                            params: {
                              comparison: '>=',
                              limit: 0,
                              exclusive: false
                            },
                            message: 'should be >= 0'
                          }];
                          return false;
                        }
                      }
                    } else {
                      MPBibliographicName.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.updatedAt',
                        schemaPath: 'numbers.json#/definitions/timestamp/type',
                        params: {
                          type: 'number'
                        },
                        message: 'should be number'
                      }];
                      return false;
                    }
                    var valid2 = errors === errs_2;
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data.objectType;
                    if (data1 === undefined) {
                      valid1 = false;
                      MPBibliographicName.errors = [{
                        keyword: 'required',
                        dataPath: (dataPath || '') + "",
                        schemaPath: '#/required',
                        params: {
                          missingProperty: 'objectType'
                        },
                        message: 'should have required property \'objectType\''
                      }];
                      return false;
                    } else {
                      var errs_1 = errors;
                      if (typeof data1 !== "string") {
                        MPBibliographicName.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '.objectType',
                          schemaPath: '#/properties/objectType/type',
                          params: {
                            type: 'string'
                          },
                          message: 'should be string'
                        }];
                        return false;
                      }
                      var schema1 = MPBibliographicName.schema.properties.objectType.enum;
                      var valid1;
                      valid1 = false;
                      for (var i1 = 0; i1 < schema1.length; i1++)
                        if (equal(data1, schema1[i1])) {
                          valid1 = true;
                          break;
                        }
                      if (!valid1) {
                        MPBibliographicName.errors = [{
                          keyword: 'enum',
                          dataPath: (dataPath || '') + '.objectType',
                          schemaPath: '#/properties/objectType/enum',
                          params: {
                            allowedValues: schema1
                          },
                          message: 'should be equal to one of the allowed values'
                        }];
                        return false;
                      }
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      if (data.family === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        if (typeof data.family !== "string") {
                          MPBibliographicName.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.family',
                            schemaPath: '#/properties/family/type',
                            params: {
                              type: 'string'
                            },
                            message: 'should be string'
                          }];
                          return false;
                        }
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        if (data.given === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          if (typeof data.given !== "string") {
                            MPBibliographicName.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.given',
                              schemaPath: '#/properties/given/type',
                              params: {
                                type: 'string'
                              },
                              message: 'should be string'
                            }];
                            return false;
                          }
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          if (data['dropping-particle'] === undefined) {
                            valid1 = true;
                          } else {
                            var errs_1 = errors;
                            if (typeof data['dropping-particle'] !== "string") {
                              MPBibliographicName.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '[\'dropping-particle\']',
                                schemaPath: '#/properties/dropping-particle/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            if (data['non-dropping-particle'] === undefined) {
                              valid1 = true;
                            } else {
                              var errs_1 = errors;
                              if (typeof data['non-dropping-particle'] !== "string") {
                                MPBibliographicName.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '[\'non-dropping-particle\']',
                                  schemaPath: '#/properties/non-dropping-particle/type',
                                  params: {
                                    type: 'string'
                                  },
                                  message: 'should be string'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              if (data.suffix === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if (typeof data.suffix !== "string") {
                                  MPBibliographicName.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.suffix',
                                    schemaPath: '#/properties/suffix/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.literal === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.literal !== "string") {
                                    MPBibliographicName.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.literal',
                                      schemaPath: '#/properties/literal/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPBibliographicName.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPBibliographicName.errors = vErrors;
      return errors === 0;
    };
    MPBibliographicName.schema = {
      "$id": "MPBibliographicName.json",
      "additionalProperties": true,
      "properties": {
        "_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?BibliographicName"
          }]
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "objectType": {
          "type": "string",
          "enum": ["BibliographicName", "MPBibliographicName"]
        },
        "family": {
          "type": "string"
        },
        "given": {
          "type": "string"
        },
        "dropping-particle": {
          "type": "string"
        },
        "non-dropping-particle": {
          "type": "string"
        },
        "suffix": {
          "type": "string"
        },
        "literal": {
          "type": "string"
        }
      },
      "required": ["_id", "objectType"],
      "type": "object"
    };
    MPBibliographicName.errors = null;

    function MPBibliographyItem(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?Library');
      var pattern2 = new RegExp('^(MP)?BibliographyItem');
      var pattern3 = new RegExp('^MPKeyword:[0-9a-zA-Z\\-]+');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      };
      var refVal3 = MPBibliographicName;
      var refVal4 = MPBibliographicDate;
      var refVal5 = {
        "type": "string",
        "pattern": "^MPKeyword:[0-9a-zA-Z\\-]+"
      }; /*# sourceURL=MPBibliographyItem.json */
      var vErrors = null;
      var errors = 0;
      if (rootData === undefined) rootData = data;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || MPBibliographyItem.schema.properties.hasOwnProperty(key0));
            if (isAdditional0) {
              valid1 = false;
              MPBibliographyItem.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data.library_id;
            if (data1 === undefined) {
              valid1 = false;
              MPBibliographyItem.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: 'library_id'
                },
                message: 'should have required property \'library_id\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPBibliographyItem.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '.library_id',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPBibliographyItem.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '.library_id',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPBibliographyItem.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.library_id',
                      schemaPath: '#/properties/library_id/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?Library'
                      },
                      message: 'should match pattern "^(MP)?Library"'
                    }];
                    return false;
                  }
                } else {
                  MPBibliographyItem.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.library_id',
                    schemaPath: '#/properties/library_id/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              var data1 = data._id;
              if (data1 === undefined) {
                valid1 = false;
                MPBibliographyItem.errors = [{
                  keyword: 'required',
                  dataPath: (dataPath || '') + "",
                  schemaPath: '#/required',
                  params: {
                    missingProperty: '_id'
                  },
                  message: 'should have required property \'_id\''
                }];
                return false;
              } else {
                var errs_1 = errors;
                var errs_2 = errors;
                var errs_3 = errors;
                if (typeof data1 === "string") {
                  if (!pattern0.test(data1)) {
                    MPBibliographyItem.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: 'strings.json#/definitions/_id/pattern',
                      params: {
                        pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                      },
                      message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                    }];
                    return false;
                  }
                } else {
                  MPBibliographyItem.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: 'strings.json#/definitions/_id/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid3 = errors === errs_3;
                var valid2 = errors === errs_2;
                if (valid2) {
                  var errs_2 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern2.test(data1)) {
                      MPBibliographyItem.errors = [{
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '._id',
                        schemaPath: '#/properties/_id/allOf/1/pattern',
                        params: {
                          pattern: '^(MP)?BibliographyItem'
                        },
                        message: 'should match pattern "^(MP)?BibliographyItem"'
                      }];
                      return false;
                    }
                  } else {
                    MPBibliographyItem.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: '#/properties/_id/allOf/1/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    }];
                    return false;
                  }
                  var valid2 = errors === errs_2;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                if (data._rev === undefined) {
                  valid1 = true;
                } else {
                  var errs_1 = errors;
                  if (typeof data._rev !== "string") {
                    MPBibliographyItem.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._rev',
                      schemaPath: '#/properties/_rev/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    }];
                    return false;
                  }
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  if (data.favorited === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    if (typeof data.favorited !== "boolean") {
                      MPBibliographyItem.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.favorited',
                        schemaPath: '#/properties/favorited/type',
                        params: {
                          type: 'boolean'
                        },
                        message: 'should be boolean'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data._revisions;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                        var errs__1 = errors;
                        var valid2 = true;
                        var data2 = data1.start;
                        if (data2 === undefined) {
                          valid2 = true;
                        } else {
                          var errs_2 = errors;
                          if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                            MPBibliographyItem.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '._revisions.start',
                              schemaPath: '#/properties/_revisions/properties/start/type',
                              params: {
                                type: 'integer'
                              },
                              message: 'should be integer'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                        }
                        if (valid2) {
                          var data2 = data1.ids;
                          if (data2 === undefined) {
                            valid2 = true;
                          } else {
                            var errs_2 = errors;
                            if (Array.isArray(data2)) {
                              var errs__2 = errors;
                              var valid2;
                              for (var i2 = 0; i2 < data2.length; i2++) {
                                var errs_3 = errors;
                                if (typeof data2[i2] !== "string") {
                                  MPBibliographyItem.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                    schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid3 = errors === errs_3;
                                if (!valid3) break;
                              }
                            } else {
                              MPBibliographyItem.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids',
                                schemaPath: '#/properties/_revisions/properties/ids/type',
                                params: {
                                  type: 'array'
                                },
                                message: 'should be array'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                          }
                        }
                      } else {
                        MPBibliographyItem.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions',
                          schemaPath: '#/properties/_revisions/type',
                          params: {
                            type: 'object'
                          },
                          message: 'should be object'
                        }];
                        return false;
                      }
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      var data1 = data.createdAt;
                      if (data1 === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        var errs_2 = errors;
                        if (typeof data1 === "number") {
                          if (data1 > 2000000000 || data1 !== data1) {
                            MPBibliographyItem.errors = [{
                              keyword: 'maximum',
                              dataPath: (dataPath || '') + '.createdAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                              params: {
                                comparison: '<=',
                                limit: 2000000000,
                                exclusive: false
                              },
                              message: 'should be <= 2000000000'
                            }];
                            return false;
                          } else {
                            if (data1 < 0 || data1 !== data1) {
                              MPBibliographyItem.errors = [{
                                keyword: 'minimum',
                                dataPath: (dataPath || '') + '.createdAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                params: {
                                  comparison: '>=',
                                  limit: 0,
                                  exclusive: false
                                },
                                message: 'should be >= 0'
                              }];
                              return false;
                            }
                          }
                        } else {
                          MPBibliographyItem.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.createdAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/type',
                            params: {
                              type: 'number'
                            },
                            message: 'should be number'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data.updatedAt;
                        if (data1 === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          var errs_2 = errors;
                          if (typeof data1 === "number") {
                            if (data1 > 2000000000 || data1 !== data1) {
                              MPBibliographyItem.errors = [{
                                keyword: 'maximum',
                                dataPath: (dataPath || '') + '.updatedAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                params: {
                                  comparison: '<=',
                                  limit: 2000000000,
                                  exclusive: false
                                },
                                message: 'should be <= 2000000000'
                              }];
                              return false;
                            } else {
                              if (data1 < 0 || data1 !== data1) {
                                MPBibliographyItem.errors = [{
                                  keyword: 'minimum',
                                  dataPath: (dataPath || '') + '.updatedAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                  params: {
                                    comparison: '>=',
                                    limit: 0,
                                    exclusive: false
                                  },
                                  message: 'should be >= 0'
                                }];
                                return false;
                              }
                            }
                          } else {
                            MPBibliographyItem.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.updatedAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/type',
                              params: {
                                type: 'number'
                              },
                              message: 'should be number'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          var data1 = data.objectType;
                          if (data1 === undefined) {
                            valid1 = false;
                            MPBibliographyItem.errors = [{
                              keyword: 'required',
                              dataPath: (dataPath || '') + "",
                              schemaPath: '#/required',
                              params: {
                                missingProperty: 'objectType'
                              },
                              message: 'should have required property \'objectType\''
                            }];
                            return false;
                          } else {
                            var errs_1 = errors;
                            if (typeof data1 !== "string") {
                              MPBibliographyItem.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.objectType',
                                schemaPath: '#/properties/objectType/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var schema1 = MPBibliographyItem.schema.properties.objectType.enum;
                            var valid1;
                            valid1 = false;
                            for (var i1 = 0; i1 < schema1.length; i1++)
                              if (equal(data1, schema1[i1])) {
                                valid1 = true;
                                break;
                              }
                            if (!valid1) {
                              MPBibliographyItem.errors = [{
                                keyword: 'enum',
                                dataPath: (dataPath || '') + '.objectType',
                                schemaPath: '#/properties/objectType/enum',
                                params: {
                                  allowedValues: schema1
                                },
                                message: 'should be equal to one of the allowed values'
                              }];
                              return false;
                            }
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            var data1 = data.type;
                            if (data1 === undefined) {
                              valid1 = false;
                              MPBibliographyItem.errors = [{
                                keyword: 'required',
                                dataPath: (dataPath || '') + "",
                                schemaPath: '#/required',
                                params: {
                                  missingProperty: 'type'
                                },
                                message: 'should have required property \'type\''
                              }];
                              return false;
                            } else {
                              var errs_1 = errors;
                              if (typeof data1 !== "string") {
                                MPBibliographyItem.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.type',
                                  schemaPath: '#/properties/type/type',
                                  params: {
                                    type: 'string'
                                  },
                                  message: 'should be string'
                                }];
                                return false;
                              }
                              var schema1 = MPBibliographyItem.schema.properties.type.enum;
                              var valid1;
                              valid1 = false;
                              for (var i1 = 0; i1 < schema1.length; i1++)
                                if (equal(data1, schema1[i1])) {
                                  valid1 = true;
                                  break;
                                }
                              if (!valid1) {
                                MPBibliographyItem.errors = [{
                                  keyword: 'enum',
                                  dataPath: (dataPath || '') + '.type',
                                  schemaPath: '#/properties/type/enum',
                                  params: {
                                    allowedValues: schema1
                                  },
                                  message: 'should be equal to one of the allowed values'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              var data1 = data.categories;
                              if (data1 === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if (Array.isArray(data1)) {
                                  var errs__1 = errors;
                                  var valid1;
                                  for (var i1 = 0; i1 < data1.length; i1++) {
                                    var errs_2 = errors;
                                    if (typeof data1[i1] !== "string") {
                                      MPBibliographyItem.errors = [{
                                        keyword: 'type',
                                        dataPath: (dataPath || '') + '.categories[' + i1 + ']',
                                        schemaPath: '#/properties/categories/items/type',
                                        params: {
                                          type: 'string'
                                        },
                                        message: 'should be string'
                                      }];
                                      return false;
                                    }
                                    var valid2 = errors === errs_2;
                                    if (!valid2) break;
                                  }
                                } else {
                                  MPBibliographyItem.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.categories',
                                    schemaPath: '#/properties/categories/type',
                                    params: {
                                      type: 'array'
                                    },
                                    message: 'should be array'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.language === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.language !== "string") {
                                    MPBibliographyItem.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.language',
                                      schemaPath: '#/properties/language/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                                if (valid1) {
                                  if (data.journalAbbreviation === undefined) {
                                    valid1 = true;
                                  } else {
                                    var errs_1 = errors;
                                    if (typeof data.journalAbbreviation !== "string") {
                                      MPBibliographyItem.errors = [{
                                        keyword: 'type',
                                        dataPath: (dataPath || '') + '.journalAbbreviation',
                                        schemaPath: '#/properties/journalAbbreviation/type',
                                        params: {
                                          type: 'string'
                                        },
                                        message: 'should be string'
                                      }];
                                      return false;
                                    }
                                    var valid1 = errors === errs_1;
                                  }
                                  if (valid1) {
                                    if (data.shortTitle === undefined) {
                                      valid1 = true;
                                    } else {
                                      var errs_1 = errors;
                                      if (typeof data.shortTitle !== "string") {
                                        MPBibliographyItem.errors = [{
                                          keyword: 'type',
                                          dataPath: (dataPath || '') + '.shortTitle',
                                          schemaPath: '#/properties/shortTitle/type',
                                          params: {
                                            type: 'string'
                                          },
                                          message: 'should be string'
                                        }];
                                        return false;
                                      }
                                      var valid1 = errors === errs_1;
                                    }
                                    if (valid1) {
                                      var data1 = data.author;
                                      if (data1 === undefined) {
                                        valid1 = true;
                                      } else {
                                        var errs_1 = errors;
                                        if (Array.isArray(data1)) {
                                          var errs__1 = errors;
                                          var valid1;
                                          for (var i1 = 0; i1 < data1.length; i1++) {
                                            var errs_2 = errors;
                                            if (!refVal3(data1[i1], (dataPath || '') + '.author[' + i1 + ']', data1, i1, rootData)) {
                                              if (vErrors === null) vErrors = refVal3.errors;
                                              else vErrors = vErrors.concat(refVal3.errors);
                                              errors = vErrors.length;
                                            }
                                            var valid2 = errors === errs_2;
                                            if (!valid2) break;
                                          }
                                        } else {
                                          MPBibliographyItem.errors = [{
                                            keyword: 'type',
                                            dataPath: (dataPath || '') + '.author',
                                            schemaPath: '#/properties/author/type',
                                            params: {
                                              type: 'array'
                                            },
                                            message: 'should be array'
                                          }];
                                          return false;
                                        }
                                        var valid1 = errors === errs_1;
                                      }
                                      if (valid1) {
                                        var data1 = data['collection-editor'];
                                        if (data1 === undefined) {
                                          valid1 = true;
                                        } else {
                                          var errs_1 = errors;
                                          if (Array.isArray(data1)) {
                                            var errs__1 = errors;
                                            var valid1;
                                            for (var i1 = 0; i1 < data1.length; i1++) {
                                              var errs_2 = errors;
                                              if (!refVal3(data1[i1], (dataPath || '') + '[\'collection-editor\'][' + i1 + ']', data1, i1, rootData)) {
                                                if (vErrors === null) vErrors = refVal3.errors;
                                                else vErrors = vErrors.concat(refVal3.errors);
                                                errors = vErrors.length;
                                              }
                                              var valid2 = errors === errs_2;
                                              if (!valid2) break;
                                            }
                                          } else {
                                            MPBibliographyItem.errors = [{
                                              keyword: 'type',
                                              dataPath: (dataPath || '') + '[\'collection-editor\']',
                                              schemaPath: '#/properties/collection-editor/type',
                                              params: {
                                                type: 'array'
                                              },
                                              message: 'should be array'
                                            }];
                                            return false;
                                          }
                                          var valid1 = errors === errs_1;
                                        }
                                        if (valid1) {
                                          var data1 = data.composer;
                                          if (data1 === undefined) {
                                            valid1 = true;
                                          } else {
                                            var errs_1 = errors;
                                            if (Array.isArray(data1)) {
                                              var errs__1 = errors;
                                              var valid1;
                                              for (var i1 = 0; i1 < data1.length; i1++) {
                                                var errs_2 = errors;
                                                if (!refVal3(data1[i1], (dataPath || '') + '.composer[' + i1 + ']', data1, i1, rootData)) {
                                                  if (vErrors === null) vErrors = refVal3.errors;
                                                  else vErrors = vErrors.concat(refVal3.errors);
                                                  errors = vErrors.length;
                                                }
                                                var valid2 = errors === errs_2;
                                                if (!valid2) break;
                                              }
                                            } else {
                                              MPBibliographyItem.errors = [{
                                                keyword: 'type',
                                                dataPath: (dataPath || '') + '.composer',
                                                schemaPath: '#/properties/composer/type',
                                                params: {
                                                  type: 'array'
                                                },
                                                message: 'should be array'
                                              }];
                                              return false;
                                            }
                                            var valid1 = errors === errs_1;
                                          }
                                          if (valid1) {
                                            var data1 = data['container-author'];
                                            if (data1 === undefined) {
                                              valid1 = true;
                                            } else {
                                              var errs_1 = errors;
                                              if (Array.isArray(data1)) {
                                                var errs__1 = errors;
                                                var valid1;
                                                for (var i1 = 0; i1 < data1.length; i1++) {
                                                  var errs_2 = errors;
                                                  if (!refVal3(data1[i1], (dataPath || '') + '[\'container-author\'][' + i1 + ']', data1, i1, rootData)) {
                                                    if (vErrors === null) vErrors = refVal3.errors;
                                                    else vErrors = vErrors.concat(refVal3.errors);
                                                    errors = vErrors.length;
                                                  }
                                                  var valid2 = errors === errs_2;
                                                  if (!valid2) break;
                                                }
                                              } else {
                                                MPBibliographyItem.errors = [{
                                                  keyword: 'type',
                                                  dataPath: (dataPath || '') + '[\'container-author\']',
                                                  schemaPath: '#/properties/container-author/type',
                                                  params: {
                                                    type: 'array'
                                                  },
                                                  message: 'should be array'
                                                }];
                                                return false;
                                              }
                                              var valid1 = errors === errs_1;
                                            }
                                            if (valid1) {
                                              var data1 = data.director;
                                              if (data1 === undefined) {
                                                valid1 = true;
                                              } else {
                                                var errs_1 = errors;
                                                if (Array.isArray(data1)) {
                                                  var errs__1 = errors;
                                                  var valid1;
                                                  for (var i1 = 0; i1 < data1.length; i1++) {
                                                    var errs_2 = errors;
                                                    if (!refVal3(data1[i1], (dataPath || '') + '.director[' + i1 + ']', data1, i1, rootData)) {
                                                      if (vErrors === null) vErrors = refVal3.errors;
                                                      else vErrors = vErrors.concat(refVal3.errors);
                                                      errors = vErrors.length;
                                                    }
                                                    var valid2 = errors === errs_2;
                                                    if (!valid2) break;
                                                  }
                                                } else {
                                                  MPBibliographyItem.errors = [{
                                                    keyword: 'type',
                                                    dataPath: (dataPath || '') + '.director',
                                                    schemaPath: '#/properties/director/type',
                                                    params: {
                                                      type: 'array'
                                                    },
                                                    message: 'should be array'
                                                  }];
                                                  return false;
                                                }
                                                var valid1 = errors === errs_1;
                                              }
                                              if (valid1) {
                                                var data1 = data.editor;
                                                if (data1 === undefined) {
                                                  valid1 = true;
                                                } else {
                                                  var errs_1 = errors;
                                                  if (Array.isArray(data1)) {
                                                    var errs__1 = errors;
                                                    var valid1;
                                                    for (var i1 = 0; i1 < data1.length; i1++) {
                                                      var errs_2 = errors;
                                                      if (!refVal3(data1[i1], (dataPath || '') + '.editor[' + i1 + ']', data1, i1, rootData)) {
                                                        if (vErrors === null) vErrors = refVal3.errors;
                                                        else vErrors = vErrors.concat(refVal3.errors);
                                                        errors = vErrors.length;
                                                      }
                                                      var valid2 = errors === errs_2;
                                                      if (!valid2) break;
                                                    }
                                                  } else {
                                                    MPBibliographyItem.errors = [{
                                                      keyword: 'type',
                                                      dataPath: (dataPath || '') + '.editor',
                                                      schemaPath: '#/properties/editor/type',
                                                      params: {
                                                        type: 'array'
                                                      },
                                                      message: 'should be array'
                                                    }];
                                                    return false;
                                                  }
                                                  var valid1 = errors === errs_1;
                                                }
                                                if (valid1) {
                                                  var data1 = data['editorial-director'];
                                                  if (data1 === undefined) {
                                                    valid1 = true;
                                                  } else {
                                                    var errs_1 = errors;
                                                    if (Array.isArray(data1)) {
                                                      var errs__1 = errors;
                                                      var valid1;
                                                      for (var i1 = 0; i1 < data1.length; i1++) {
                                                        var errs_2 = errors;
                                                        if (!refVal3(data1[i1], (dataPath || '') + '[\'editorial-director\'][' + i1 + ']', data1, i1, rootData)) {
                                                          if (vErrors === null) vErrors = refVal3.errors;
                                                          else vErrors = vErrors.concat(refVal3.errors);
                                                          errors = vErrors.length;
                                                        }
                                                        var valid2 = errors === errs_2;
                                                        if (!valid2) break;
                                                      }
                                                    } else {
                                                      MPBibliographyItem.errors = [{
                                                        keyword: 'type',
                                                        dataPath: (dataPath || '') + '[\'editorial-director\']',
                                                        schemaPath: '#/properties/editorial-director/type',
                                                        params: {
                                                          type: 'array'
                                                        },
                                                        message: 'should be array'
                                                      }];
                                                      return false;
                                                    }
                                                    var valid1 = errors === errs_1;
                                                  }
                                                  if (valid1) {
                                                    var data1 = data.interviewer;
                                                    if (data1 === undefined) {
                                                      valid1 = true;
                                                    } else {
                                                      var errs_1 = errors;
                                                      if (Array.isArray(data1)) {
                                                        var errs__1 = errors;
                                                        var valid1;
                                                        for (var i1 = 0; i1 < data1.length; i1++) {
                                                          var errs_2 = errors;
                                                          if (!refVal3(data1[i1], (dataPath || '') + '.interviewer[' + i1 + ']', data1, i1, rootData)) {
                                                            if (vErrors === null) vErrors = refVal3.errors;
                                                            else vErrors = vErrors.concat(refVal3.errors);
                                                            errors = vErrors.length;
                                                          }
                                                          var valid2 = errors === errs_2;
                                                          if (!valid2) break;
                                                        }
                                                      } else {
                                                        MPBibliographyItem.errors = [{
                                                          keyword: 'type',
                                                          dataPath: (dataPath || '') + '.interviewer',
                                                          schemaPath: '#/properties/interviewer/type',
                                                          params: {
                                                            type: 'array'
                                                          },
                                                          message: 'should be array'
                                                        }];
                                                        return false;
                                                      }
                                                      var valid1 = errors === errs_1;
                                                    }
                                                    if (valid1) {
                                                      var data1 = data.illustrator;
                                                      if (data1 === undefined) {
                                                        valid1 = true;
                                                      } else {
                                                        var errs_1 = errors;
                                                        if (Array.isArray(data1)) {
                                                          var errs__1 = errors;
                                                          var valid1;
                                                          for (var i1 = 0; i1 < data1.length; i1++) {
                                                            var errs_2 = errors;
                                                            if (!refVal3(data1[i1], (dataPath || '') + '.illustrator[' + i1 + ']', data1, i1, rootData)) {
                                                              if (vErrors === null) vErrors = refVal3.errors;
                                                              else vErrors = vErrors.concat(refVal3.errors);
                                                              errors = vErrors.length;
                                                            }
                                                            var valid2 = errors === errs_2;
                                                            if (!valid2) break;
                                                          }
                                                        } else {
                                                          MPBibliographyItem.errors = [{
                                                            keyword: 'type',
                                                            dataPath: (dataPath || '') + '.illustrator',
                                                            schemaPath: '#/properties/illustrator/type',
                                                            params: {
                                                              type: 'array'
                                                            },
                                                            message: 'should be array'
                                                          }];
                                                          return false;
                                                        }
                                                        var valid1 = errors === errs_1;
                                                      }
                                                      if (valid1) {
                                                        var data1 = data['original-author'];
                                                        if (data1 === undefined) {
                                                          valid1 = true;
                                                        } else {
                                                          var errs_1 = errors;
                                                          if (Array.isArray(data1)) {
                                                            var errs__1 = errors;
                                                            var valid1;
                                                            for (var i1 = 0; i1 < data1.length; i1++) {
                                                              var errs_2 = errors;
                                                              if (!refVal3(data1[i1], (dataPath || '') + '[\'original-author\'][' + i1 + ']', data1, i1, rootData)) {
                                                                if (vErrors === null) vErrors = refVal3.errors;
                                                                else vErrors = vErrors.concat(refVal3.errors);
                                                                errors = vErrors.length;
                                                              }
                                                              var valid2 = errors === errs_2;
                                                              if (!valid2) break;
                                                            }
                                                          } else {
                                                            MPBibliographyItem.errors = [{
                                                              keyword: 'type',
                                                              dataPath: (dataPath || '') + '[\'original-author\']',
                                                              schemaPath: '#/properties/original-author/type',
                                                              params: {
                                                                type: 'array'
                                                              },
                                                              message: 'should be array'
                                                            }];
                                                            return false;
                                                          }
                                                          var valid1 = errors === errs_1;
                                                        }
                                                        if (valid1) {
                                                          var data1 = data.recipient;
                                                          if (data1 === undefined) {
                                                            valid1 = true;
                                                          } else {
                                                            var errs_1 = errors;
                                                            if (Array.isArray(data1)) {
                                                              var errs__1 = errors;
                                                              var valid1;
                                                              for (var i1 = 0; i1 < data1.length; i1++) {
                                                                var errs_2 = errors;
                                                                if (!refVal3(data1[i1], (dataPath || '') + '.recipient[' + i1 + ']', data1, i1, rootData)) {
                                                                  if (vErrors === null) vErrors = refVal3.errors;
                                                                  else vErrors = vErrors.concat(refVal3.errors);
                                                                  errors = vErrors.length;
                                                                }
                                                                var valid2 = errors === errs_2;
                                                                if (!valid2) break;
                                                              }
                                                            } else {
                                                              MPBibliographyItem.errors = [{
                                                                keyword: 'type',
                                                                dataPath: (dataPath || '') + '.recipient',
                                                                schemaPath: '#/properties/recipient/type',
                                                                params: {
                                                                  type: 'array'
                                                                },
                                                                message: 'should be array'
                                                              }];
                                                              return false;
                                                            }
                                                            var valid1 = errors === errs_1;
                                                          }
                                                          if (valid1) {
                                                            var data1 = data['reviewed-author'];
                                                            if (data1 === undefined) {
                                                              valid1 = true;
                                                            } else {
                                                              var errs_1 = errors;
                                                              if (Array.isArray(data1)) {
                                                                var errs__1 = errors;
                                                                var valid1;
                                                                for (var i1 = 0; i1 < data1.length; i1++) {
                                                                  var errs_2 = errors;
                                                                  if (!refVal3(data1[i1], (dataPath || '') + '[\'reviewed-author\'][' + i1 + ']', data1, i1, rootData)) {
                                                                    if (vErrors === null) vErrors = refVal3.errors;
                                                                    else vErrors = vErrors.concat(refVal3.errors);
                                                                    errors = vErrors.length;
                                                                  }
                                                                  var valid2 = errors === errs_2;
                                                                  if (!valid2) break;
                                                                }
                                                              } else {
                                                                MPBibliographyItem.errors = [{
                                                                  keyword: 'type',
                                                                  dataPath: (dataPath || '') + '[\'reviewed-author\']',
                                                                  schemaPath: '#/properties/reviewed-author/type',
                                                                  params: {
                                                                    type: 'array'
                                                                  },
                                                                  message: 'should be array'
                                                                }];
                                                                return false;
                                                              }
                                                              var valid1 = errors === errs_1;
                                                            }
                                                            if (valid1) {
                                                              var data1 = data.translator;
                                                              if (data1 === undefined) {
                                                                valid1 = true;
                                                              } else {
                                                                var errs_1 = errors;
                                                                if (Array.isArray(data1)) {
                                                                  var errs__1 = errors;
                                                                  var valid1;
                                                                  for (var i1 = 0; i1 < data1.length; i1++) {
                                                                    var errs_2 = errors;
                                                                    if (!refVal3(data1[i1], (dataPath || '') + '.translator[' + i1 + ']', data1, i1, rootData)) {
                                                                      if (vErrors === null) vErrors = refVal3.errors;
                                                                      else vErrors = vErrors.concat(refVal3.errors);
                                                                      errors = vErrors.length;
                                                                    }
                                                                    var valid2 = errors === errs_2;
                                                                    if (!valid2) break;
                                                                  }
                                                                } else {
                                                                  MPBibliographyItem.errors = [{
                                                                    keyword: 'type',
                                                                    dataPath: (dataPath || '') + '.translator',
                                                                    schemaPath: '#/properties/translator/type',
                                                                    params: {
                                                                      type: 'array'
                                                                    },
                                                                    message: 'should be array'
                                                                  }];
                                                                  return false;
                                                                }
                                                                var valid1 = errors === errs_1;
                                                              }
                                                              if (valid1) {
                                                                if (data.accessed === undefined) {
                                                                  valid1 = true;
                                                                } else {
                                                                  var errs_1 = errors;
                                                                  if (!refVal4(data.accessed, (dataPath || '') + '.accessed', data, 'accessed', rootData)) {
                                                                    if (vErrors === null) vErrors = refVal4.errors;
                                                                    else vErrors = vErrors.concat(refVal4.errors);
                                                                    errors = vErrors.length;
                                                                  }
                                                                  var valid1 = errors === errs_1;
                                                                }
                                                                if (valid1) {
                                                                  if (data.container === undefined) {
                                                                    valid1 = true;
                                                                  } else {
                                                                    var errs_1 = errors;
                                                                    if (!refVal4(data.container, (dataPath || '') + '.container', data, 'container', rootData)) {
                                                                      if (vErrors === null) vErrors = refVal4.errors;
                                                                      else vErrors = vErrors.concat(refVal4.errors);
                                                                      errors = vErrors.length;
                                                                    }
                                                                    var valid1 = errors === errs_1;
                                                                  }
                                                                  if (valid1) {
                                                                    if (data['event-date'] === undefined) {
                                                                      valid1 = true;
                                                                    } else {
                                                                      var errs_1 = errors;
                                                                      if (!refVal4(data['event-date'], (dataPath || '') + '[\'event-date\']', data, 'event-date', rootData)) {
                                                                        if (vErrors === null) vErrors = refVal4.errors;
                                                                        else vErrors = vErrors.concat(refVal4.errors);
                                                                        errors = vErrors.length;
                                                                      }
                                                                      var valid1 = errors === errs_1;
                                                                    }
                                                                    if (valid1) {
                                                                      if (data.issued === undefined) {
                                                                        valid1 = true;
                                                                      } else {
                                                                        var errs_1 = errors;
                                                                        if (!refVal4(data.issued, (dataPath || '') + '.issued', data, 'issued', rootData)) {
                                                                          if (vErrors === null) vErrors = refVal4.errors;
                                                                          else vErrors = vErrors.concat(refVal4.errors);
                                                                          errors = vErrors.length;
                                                                        }
                                                                        var valid1 = errors === errs_1;
                                                                      }
                                                                      if (valid1) {
                                                                        if (data['original-date'] === undefined) {
                                                                          valid1 = true;
                                                                        } else {
                                                                          var errs_1 = errors;
                                                                          if (!refVal4(data['original-date'], (dataPath || '') + '[\'original-date\']', data, 'original-date', rootData)) {
                                                                            if (vErrors === null) vErrors = refVal4.errors;
                                                                            else vErrors = vErrors.concat(refVal4.errors);
                                                                            errors = vErrors.length;
                                                                          }
                                                                          var valid1 = errors === errs_1;
                                                                        }
                                                                        if (valid1) {
                                                                          if (data.submitted === undefined) {
                                                                            valid1 = true;
                                                                          } else {
                                                                            var errs_1 = errors;
                                                                            if (!refVal4(data.submitted, (dataPath || '') + '.submitted', data, 'submitted', rootData)) {
                                                                              if (vErrors === null) vErrors = refVal4.errors;
                                                                              else vErrors = vErrors.concat(refVal4.errors);
                                                                              errors = vErrors.length;
                                                                            }
                                                                            var valid1 = errors === errs_1;
                                                                          }
                                                                          if (valid1) {
                                                                            if (data.abstract === undefined) {
                                                                              valid1 = true;
                                                                            } else {
                                                                              var errs_1 = errors;
                                                                              if (typeof data.abstract !== "string") {
                                                                                MPBibliographyItem.errors = [{
                                                                                  keyword: 'type',
                                                                                  dataPath: (dataPath || '') + '.abstract',
                                                                                  schemaPath: '#/properties/abstract/type',
                                                                                  params: {
                                                                                    type: 'string'
                                                                                  },
                                                                                  message: 'should be string'
                                                                                }];
                                                                                return false;
                                                                              }
                                                                              var valid1 = errors === errs_1;
                                                                            }
                                                                            if (valid1) {
                                                                              if (data.annote === undefined) {
                                                                                valid1 = true;
                                                                              } else {
                                                                                var errs_1 = errors;
                                                                                if (typeof data.annote !== "string") {
                                                                                  MPBibliographyItem.errors = [{
                                                                                    keyword: 'type',
                                                                                    dataPath: (dataPath || '') + '.annote',
                                                                                    schemaPath: '#/properties/annote/type',
                                                                                    params: {
                                                                                      type: 'string'
                                                                                    },
                                                                                    message: 'should be string'
                                                                                  }];
                                                                                  return false;
                                                                                }
                                                                                var valid1 = errors === errs_1;
                                                                              }
                                                                              if (valid1) {
                                                                                if (data.archive === undefined) {
                                                                                  valid1 = true;
                                                                                } else {
                                                                                  var errs_1 = errors;
                                                                                  if (typeof data.archive !== "string") {
                                                                                    MPBibliographyItem.errors = [{
                                                                                      keyword: 'type',
                                                                                      dataPath: (dataPath || '') + '.archive',
                                                                                      schemaPath: '#/properties/archive/type',
                                                                                      params: {
                                                                                        type: 'string'
                                                                                      },
                                                                                      message: 'should be string'
                                                                                    }];
                                                                                    return false;
                                                                                  }
                                                                                  var valid1 = errors === errs_1;
                                                                                }
                                                                                if (valid1) {
                                                                                  if (data.archive_location === undefined) {
                                                                                    valid1 = true;
                                                                                  } else {
                                                                                    var errs_1 = errors;
                                                                                    if (typeof data.archive_location !== "string") {
                                                                                      MPBibliographyItem.errors = [{
                                                                                        keyword: 'type',
                                                                                        dataPath: (dataPath || '') + '.archive_location',
                                                                                        schemaPath: '#/properties/archive_location/type',
                                                                                        params: {
                                                                                          type: 'string'
                                                                                        },
                                                                                        message: 'should be string'
                                                                                      }];
                                                                                      return false;
                                                                                    }
                                                                                    var valid1 = errors === errs_1;
                                                                                  }
                                                                                  if (valid1) {
                                                                                    if (data['archive-place'] === undefined) {
                                                                                      valid1 = true;
                                                                                    } else {
                                                                                      var errs_1 = errors;
                                                                                      if (typeof data['archive-place'] !== "string") {
                                                                                        MPBibliographyItem.errors = [{
                                                                                          keyword: 'type',
                                                                                          dataPath: (dataPath || '') + '[\'archive-place\']',
                                                                                          schemaPath: '#/properties/archive-place/type',
                                                                                          params: {
                                                                                            type: 'string'
                                                                                          },
                                                                                          message: 'should be string'
                                                                                        }];
                                                                                        return false;
                                                                                      }
                                                                                      var valid1 = errors === errs_1;
                                                                                    }
                                                                                    if (valid1) {
                                                                                      if (data.authority === undefined) {
                                                                                        valid1 = true;
                                                                                      } else {
                                                                                        var errs_1 = errors;
                                                                                        if (typeof data.authority !== "string") {
                                                                                          MPBibliographyItem.errors = [{
                                                                                            keyword: 'type',
                                                                                            dataPath: (dataPath || '') + '.authority',
                                                                                            schemaPath: '#/properties/authority/type',
                                                                                            params: {
                                                                                              type: 'string'
                                                                                            },
                                                                                            message: 'should be string'
                                                                                          }];
                                                                                          return false;
                                                                                        }
                                                                                        var valid1 = errors === errs_1;
                                                                                      }
                                                                                      if (valid1) {
                                                                                        if (data['call-number'] === undefined) {
                                                                                          valid1 = true;
                                                                                        } else {
                                                                                          var errs_1 = errors;
                                                                                          if (typeof data['call-number'] !== "string") {
                                                                                            MPBibliographyItem.errors = [{
                                                                                              keyword: 'type',
                                                                                              dataPath: (dataPath || '') + '[\'call-number\']',
                                                                                              schemaPath: '#/properties/call-number/type',
                                                                                              params: {
                                                                                                type: 'string'
                                                                                              },
                                                                                              message: 'should be string'
                                                                                            }];
                                                                                            return false;
                                                                                          }
                                                                                          var valid1 = errors === errs_1;
                                                                                        }
                                                                                        if (valid1) {
                                                                                          var data1 = data['chapter-number'];
                                                                                          if (data1 === undefined) {
                                                                                            valid1 = true;
                                                                                          } else {
                                                                                            var errs_1 = errors;
                                                                                            if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                              MPBibliographyItem.errors = [{
                                                                                                keyword: 'type',
                                                                                                dataPath: (dataPath || '') + '[\'chapter-number\']',
                                                                                                schemaPath: '#/properties/chapter-number/type',
                                                                                                params: {
                                                                                                  type: 'integer'
                                                                                                },
                                                                                                message: 'should be integer'
                                                                                              }];
                                                                                              return false;
                                                                                            }
                                                                                            var valid1 = errors === errs_1;
                                                                                          }
                                                                                          if (valid1) {
                                                                                            var data1 = data['citation-number'];
                                                                                            if (data1 === undefined) {
                                                                                              valid1 = true;
                                                                                            } else {
                                                                                              var errs_1 = errors;
                                                                                              if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                                MPBibliographyItem.errors = [{
                                                                                                  keyword: 'type',
                                                                                                  dataPath: (dataPath || '') + '[\'citation-number\']',
                                                                                                  schemaPath: '#/properties/citation-number/type',
                                                                                                  params: {
                                                                                                    type: 'integer'
                                                                                                  },
                                                                                                  message: 'should be integer'
                                                                                                }];
                                                                                                return false;
                                                                                              }
                                                                                              var valid1 = errors === errs_1;
                                                                                            }
                                                                                            if (valid1) {
                                                                                              if (data['citation-label'] === undefined) {
                                                                                                valid1 = true;
                                                                                              } else {
                                                                                                var errs_1 = errors;
                                                                                                if (typeof data['citation-label'] !== "string") {
                                                                                                  MPBibliographyItem.errors = [{
                                                                                                    keyword: 'type',
                                                                                                    dataPath: (dataPath || '') + '[\'citation-label\']',
                                                                                                    schemaPath: '#/properties/citation-label/type',
                                                                                                    params: {
                                                                                                      type: 'string'
                                                                                                    },
                                                                                                    message: 'should be string'
                                                                                                  }];
                                                                                                  return false;
                                                                                                }
                                                                                                var valid1 = errors === errs_1;
                                                                                              }
                                                                                              if (valid1) {
                                                                                                var data1 = data['collection-number'];
                                                                                                if (data1 === undefined) {
                                                                                                  valid1 = true;
                                                                                                } else {
                                                                                                  var errs_1 = errors;
                                                                                                  if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                                    MPBibliographyItem.errors = [{
                                                                                                      keyword: 'type',
                                                                                                      dataPath: (dataPath || '') + '[\'collection-number\']',
                                                                                                      schemaPath: '#/properties/collection-number/type',
                                                                                                      params: {
                                                                                                        type: 'integer'
                                                                                                      },
                                                                                                      message: 'should be integer'
                                                                                                    }];
                                                                                                    return false;
                                                                                                  }
                                                                                                  var valid1 = errors === errs_1;
                                                                                                }
                                                                                                if (valid1) {
                                                                                                  if (data['collection-title'] === undefined) {
                                                                                                    valid1 = true;
                                                                                                  } else {
                                                                                                    var errs_1 = errors;
                                                                                                    if (typeof data['collection-title'] !== "string") {
                                                                                                      MPBibliographyItem.errors = [{
                                                                                                        keyword: 'type',
                                                                                                        dataPath: (dataPath || '') + '[\'collection-title\']',
                                                                                                        schemaPath: '#/properties/collection-title/type',
                                                                                                        params: {
                                                                                                          type: 'string'
                                                                                                        },
                                                                                                        message: 'should be string'
                                                                                                      }];
                                                                                                      return false;
                                                                                                    }
                                                                                                    var valid1 = errors === errs_1;
                                                                                                  }
                                                                                                  if (valid1) {
                                                                                                    if (data['container-title'] === undefined) {
                                                                                                      valid1 = true;
                                                                                                    } else {
                                                                                                      var errs_1 = errors;
                                                                                                      if (typeof data['container-title'] !== "string") {
                                                                                                        MPBibliographyItem.errors = [{
                                                                                                          keyword: 'type',
                                                                                                          dataPath: (dataPath || '') + '[\'container-title\']',
                                                                                                          schemaPath: '#/properties/container-title/type',
                                                                                                          params: {
                                                                                                            type: 'string'
                                                                                                          },
                                                                                                          message: 'should be string'
                                                                                                        }];
                                                                                                        return false;
                                                                                                      }
                                                                                                      var valid1 = errors === errs_1;
                                                                                                    }
                                                                                                    if (valid1) {
                                                                                                      if (data['container-title-short'] === undefined) {
                                                                                                        valid1 = true;
                                                                                                      } else {
                                                                                                        var errs_1 = errors;
                                                                                                        if (typeof data['container-title-short'] !== "string") {
                                                                                                          MPBibliographyItem.errors = [{
                                                                                                            keyword: 'type',
                                                                                                            dataPath: (dataPath || '') + '[\'container-title-short\']',
                                                                                                            schemaPath: '#/properties/container-title-short/type',
                                                                                                            params: {
                                                                                                              type: 'string'
                                                                                                            },
                                                                                                            message: 'should be string'
                                                                                                          }];
                                                                                                          return false;
                                                                                                        }
                                                                                                        var valid1 = errors === errs_1;
                                                                                                      }
                                                                                                      if (valid1) {
                                                                                                        if (data.dimensions === undefined) {
                                                                                                          valid1 = true;
                                                                                                        } else {
                                                                                                          var errs_1 = errors;
                                                                                                          if (typeof data.dimensions !== "string") {
                                                                                                            MPBibliographyItem.errors = [{
                                                                                                              keyword: 'type',
                                                                                                              dataPath: (dataPath || '') + '.dimensions',
                                                                                                              schemaPath: '#/properties/dimensions/type',
                                                                                                              params: {
                                                                                                                type: 'string'
                                                                                                              },
                                                                                                              message: 'should be string'
                                                                                                            }];
                                                                                                            return false;
                                                                                                          }
                                                                                                          var valid1 = errors === errs_1;
                                                                                                        }
                                                                                                        if (valid1) {
                                                                                                          if (data.DOI === undefined) {
                                                                                                            valid1 = true;
                                                                                                          } else {
                                                                                                            var errs_1 = errors;
                                                                                                            if (typeof data.DOI !== "string") {
                                                                                                              MPBibliographyItem.errors = [{
                                                                                                                keyword: 'type',
                                                                                                                dataPath: (dataPath || '') + '.DOI',
                                                                                                                schemaPath: '#/properties/DOI/type',
                                                                                                                params: {
                                                                                                                  type: 'string'
                                                                                                                },
                                                                                                                message: 'should be string'
                                                                                                              }];
                                                                                                              return false;
                                                                                                            }
                                                                                                            var valid1 = errors === errs_1;
                                                                                                          }
                                                                                                          if (valid1) {
                                                                                                            var data1 = data.edition;
                                                                                                            if (data1 === undefined) {
                                                                                                              valid1 = true;
                                                                                                            } else {
                                                                                                              var errs_1 = errors;
                                                                                                              if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                                                MPBibliographyItem.errors = [{
                                                                                                                  keyword: 'type',
                                                                                                                  dataPath: (dataPath || '') + '.edition',
                                                                                                                  schemaPath: '#/properties/edition/type',
                                                                                                                  params: {
                                                                                                                    type: 'integer'
                                                                                                                  },
                                                                                                                  message: 'should be integer'
                                                                                                                }];
                                                                                                                return false;
                                                                                                              }
                                                                                                              var valid1 = errors === errs_1;
                                                                                                            }
                                                                                                            if (valid1) {
                                                                                                              if (data.event === undefined) {
                                                                                                                valid1 = true;
                                                                                                              } else {
                                                                                                                var errs_1 = errors;
                                                                                                                if (typeof data.event !== "string") {
                                                                                                                  MPBibliographyItem.errors = [{
                                                                                                                    keyword: 'type',
                                                                                                                    dataPath: (dataPath || '') + '.event',
                                                                                                                    schemaPath: '#/properties/event/type',
                                                                                                                    params: {
                                                                                                                      type: 'string'
                                                                                                                    },
                                                                                                                    message: 'should be string'
                                                                                                                  }];
                                                                                                                  return false;
                                                                                                                }
                                                                                                                var valid1 = errors === errs_1;
                                                                                                              }
                                                                                                              if (valid1) {
                                                                                                                if (data['event-place'] === undefined) {
                                                                                                                  valid1 = true;
                                                                                                                } else {
                                                                                                                  var errs_1 = errors;
                                                                                                                  if (typeof data['event-place'] !== "string") {
                                                                                                                    MPBibliographyItem.errors = [{
                                                                                                                      keyword: 'type',
                                                                                                                      dataPath: (dataPath || '') + '[\'event-place\']',
                                                                                                                      schemaPath: '#/properties/event-place/type',
                                                                                                                      params: {
                                                                                                                        type: 'string'
                                                                                                                      },
                                                                                                                      message: 'should be string'
                                                                                                                    }];
                                                                                                                    return false;
                                                                                                                  }
                                                                                                                  var valid1 = errors === errs_1;
                                                                                                                }
                                                                                                                if (valid1) {
                                                                                                                  if (data['first-reference-note-number'] === undefined) {
                                                                                                                    valid1 = true;
                                                                                                                  } else {
                                                                                                                    var errs_1 = errors;
                                                                                                                    if (typeof data['first-reference-note-number'] !== "string") {
                                                                                                                      MPBibliographyItem.errors = [{
                                                                                                                        keyword: 'type',
                                                                                                                        dataPath: (dataPath || '') + '[\'first-reference-note-number\']',
                                                                                                                        schemaPath: '#/properties/first-reference-note-number/type',
                                                                                                                        params: {
                                                                                                                          type: 'string'
                                                                                                                        },
                                                                                                                        message: 'should be string'
                                                                                                                      }];
                                                                                                                      return false;
                                                                                                                    }
                                                                                                                    var valid1 = errors === errs_1;
                                                                                                                  }
                                                                                                                  if (valid1) {
                                                                                                                    if (data.genre === undefined) {
                                                                                                                      valid1 = true;
                                                                                                                    } else {
                                                                                                                      var errs_1 = errors;
                                                                                                                      if (typeof data.genre !== "string") {
                                                                                                                        MPBibliographyItem.errors = [{
                                                                                                                          keyword: 'type',
                                                                                                                          dataPath: (dataPath || '') + '.genre',
                                                                                                                          schemaPath: '#/properties/genre/type',
                                                                                                                          params: {
                                                                                                                            type: 'string'
                                                                                                                          },
                                                                                                                          message: 'should be string'
                                                                                                                        }];
                                                                                                                        return false;
                                                                                                                      }
                                                                                                                      var valid1 = errors === errs_1;
                                                                                                                    }
                                                                                                                    if (valid1) {
                                                                                                                      if (data.ISBN === undefined) {
                                                                                                                        valid1 = true;
                                                                                                                      } else {
                                                                                                                        var errs_1 = errors;
                                                                                                                        if (typeof data.ISBN !== "string") {
                                                                                                                          MPBibliographyItem.errors = [{
                                                                                                                            keyword: 'type',
                                                                                                                            dataPath: (dataPath || '') + '.ISBN',
                                                                                                                            schemaPath: '#/properties/ISBN/type',
                                                                                                                            params: {
                                                                                                                              type: 'string'
                                                                                                                            },
                                                                                                                            message: 'should be string'
                                                                                                                          }];
                                                                                                                          return false;
                                                                                                                        }
                                                                                                                        var valid1 = errors === errs_1;
                                                                                                                      }
                                                                                                                      if (valid1) {
                                                                                                                        var data1 = data.ISSN;
                                                                                                                        if (data1 === undefined) {
                                                                                                                          valid1 = true;
                                                                                                                        } else {
                                                                                                                          var errs_1 = errors;
                                                                                                                          if (typeof data1 !== "string" && !Array.isArray(data1)) {
                                                                                                                            MPBibliographyItem.errors = [{
                                                                                                                              keyword: 'type',
                                                                                                                              dataPath: (dataPath || '') + '.ISSN',
                                                                                                                              schemaPath: '#/properties/ISSN/type',
                                                                                                                              params: {
                                                                                                                                type: 'string,array'
                                                                                                                              },
                                                                                                                              message: 'should be string,array'
                                                                                                                            }];
                                                                                                                            return false;
                                                                                                                          }
                                                                                                                          var valid1 = errors === errs_1;
                                                                                                                        }
                                                                                                                        if (valid1) {
                                                                                                                          var data1 = data.issue;
                                                                                                                          if (data1 === undefined) {
                                                                                                                            valid1 = true;
                                                                                                                          } else {
                                                                                                                            var errs_1 = errors;
                                                                                                                            if (typeof data1 !== "string" && typeof data1 !== "number") {
                                                                                                                              MPBibliographyItem.errors = [{
                                                                                                                                keyword: 'type',
                                                                                                                                dataPath: (dataPath || '') + '.issue',
                                                                                                                                schemaPath: '#/properties/issue/type',
                                                                                                                                params: {
                                                                                                                                  type: 'string,number'
                                                                                                                                },
                                                                                                                                message: 'should be string,number'
                                                                                                                              }];
                                                                                                                              return false;
                                                                                                                            }
                                                                                                                            var valid1 = errors === errs_1;
                                                                                                                          }
                                                                                                                          if (valid1) {
                                                                                                                            if (data.jurisdiction === undefined) {
                                                                                                                              valid1 = true;
                                                                                                                            } else {
                                                                                                                              var errs_1 = errors;
                                                                                                                              if (typeof data.jurisdiction !== "string") {
                                                                                                                                MPBibliographyItem.errors = [{
                                                                                                                                  keyword: 'type',
                                                                                                                                  dataPath: (dataPath || '') + '.jurisdiction',
                                                                                                                                  schemaPath: '#/properties/jurisdiction/type',
                                                                                                                                  params: {
                                                                                                                                    type: 'string'
                                                                                                                                  },
                                                                                                                                  message: 'should be string'
                                                                                                                                }];
                                                                                                                                return false;
                                                                                                                              }
                                                                                                                              var valid1 = errors === errs_1;
                                                                                                                            }
                                                                                                                            if (valid1) {
                                                                                                                              if (data.keyword === undefined) {
                                                                                                                                valid1 = true;
                                                                                                                              } else {
                                                                                                                                var errs_1 = errors;
                                                                                                                                if (typeof data.keyword !== "string") {
                                                                                                                                  MPBibliographyItem.errors = [{
                                                                                                                                    keyword: 'type',
                                                                                                                                    dataPath: (dataPath || '') + '.keyword',
                                                                                                                                    schemaPath: '#/properties/keyword/type',
                                                                                                                                    params: {
                                                                                                                                      type: 'string'
                                                                                                                                    },
                                                                                                                                    message: 'should be string'
                                                                                                                                  }];
                                                                                                                                  return false;
                                                                                                                                }
                                                                                                                                var valid1 = errors === errs_1;
                                                                                                                              }
                                                                                                                              if (valid1) {
                                                                                                                                if (data.locator === undefined) {
                                                                                                                                  valid1 = true;
                                                                                                                                } else {
                                                                                                                                  var errs_1 = errors;
                                                                                                                                  if (typeof data.locator !== "string") {
                                                                                                                                    MPBibliographyItem.errors = [{
                                                                                                                                      keyword: 'type',
                                                                                                                                      dataPath: (dataPath || '') + '.locator',
                                                                                                                                      schemaPath: '#/properties/locator/type',
                                                                                                                                      params: {
                                                                                                                                        type: 'string'
                                                                                                                                      },
                                                                                                                                      message: 'should be string'
                                                                                                                                    }];
                                                                                                                                    return false;
                                                                                                                                  }
                                                                                                                                  var valid1 = errors === errs_1;
                                                                                                                                }
                                                                                                                                if (valid1) {
                                                                                                                                  if (data.medium === undefined) {
                                                                                                                                    valid1 = true;
                                                                                                                                  } else {
                                                                                                                                    var errs_1 = errors;
                                                                                                                                    if (typeof data.medium !== "string") {
                                                                                                                                      MPBibliographyItem.errors = [{
                                                                                                                                        keyword: 'type',
                                                                                                                                        dataPath: (dataPath || '') + '.medium',
                                                                                                                                        schemaPath: '#/properties/medium/type',
                                                                                                                                        params: {
                                                                                                                                          type: 'string'
                                                                                                                                        },
                                                                                                                                        message: 'should be string'
                                                                                                                                      }];
                                                                                                                                      return false;
                                                                                                                                    }
                                                                                                                                    var valid1 = errors === errs_1;
                                                                                                                                  }
                                                                                                                                  if (valid1) {
                                                                                                                                    if (data.note === undefined) {
                                                                                                                                      valid1 = true;
                                                                                                                                    } else {
                                                                                                                                      var errs_1 = errors;
                                                                                                                                      if (typeof data.note !== "string") {
                                                                                                                                        MPBibliographyItem.errors = [{
                                                                                                                                          keyword: 'type',
                                                                                                                                          dataPath: (dataPath || '') + '.note',
                                                                                                                                          schemaPath: '#/properties/note/type',
                                                                                                                                          params: {
                                                                                                                                            type: 'string'
                                                                                                                                          },
                                                                                                                                          message: 'should be string'
                                                                                                                                        }];
                                                                                                                                        return false;
                                                                                                                                      }
                                                                                                                                      var valid1 = errors === errs_1;
                                                                                                                                    }
                                                                                                                                    if (valid1) {
                                                                                                                                      var data1 = data.number;
                                                                                                                                      if (data1 === undefined) {
                                                                                                                                        valid1 = true;
                                                                                                                                      } else {
                                                                                                                                        var errs_1 = errors;
                                                                                                                                        if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                                                                          MPBibliographyItem.errors = [{
                                                                                                                                            keyword: 'type',
                                                                                                                                            dataPath: (dataPath || '') + '.number',
                                                                                                                                            schemaPath: '#/properties/number/type',
                                                                                                                                            params: {
                                                                                                                                              type: 'integer'
                                                                                                                                            },
                                                                                                                                            message: 'should be integer'
                                                                                                                                          }];
                                                                                                                                          return false;
                                                                                                                                        }
                                                                                                                                        var valid1 = errors === errs_1;
                                                                                                                                      }
                                                                                                                                      if (valid1) {
                                                                                                                                        var data1 = data['number-of-pages'];
                                                                                                                                        if (data1 === undefined) {
                                                                                                                                          valid1 = true;
                                                                                                                                        } else {
                                                                                                                                          var errs_1 = errors;
                                                                                                                                          if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                                                                            MPBibliographyItem.errors = [{
                                                                                                                                              keyword: 'type',
                                                                                                                                              dataPath: (dataPath || '') + '[\'number-of-pages\']',
                                                                                                                                              schemaPath: '#/properties/number-of-pages/type',
                                                                                                                                              params: {
                                                                                                                                                type: 'integer'
                                                                                                                                              },
                                                                                                                                              message: 'should be integer'
                                                                                                                                            }];
                                                                                                                                            return false;
                                                                                                                                          }
                                                                                                                                          var valid1 = errors === errs_1;
                                                                                                                                        }
                                                                                                                                        if (valid1) {
                                                                                                                                          var data1 = data['number-of-volumes'];
                                                                                                                                          if (data1 === undefined) {
                                                                                                                                            valid1 = true;
                                                                                                                                          } else {
                                                                                                                                            var errs_1 = errors;
                                                                                                                                            if ((typeof data1 !== "number" || (data1 % 1) || data1 !== data1)) {
                                                                                                                                              MPBibliographyItem.errors = [{
                                                                                                                                                keyword: 'type',
                                                                                                                                                dataPath: (dataPath || '') + '[\'number-of-volumes\']',
                                                                                                                                                schemaPath: '#/properties/number-of-volumes/type',
                                                                                                                                                params: {
                                                                                                                                                  type: 'integer'
                                                                                                                                                },
                                                                                                                                                message: 'should be integer'
                                                                                                                                              }];
                                                                                                                                              return false;
                                                                                                                                            }
                                                                                                                                            var valid1 = errors === errs_1;
                                                                                                                                          }
                                                                                                                                          if (valid1) {
                                                                                                                                            if (data['original-publisher'] === undefined) {
                                                                                                                                              valid1 = true;
                                                                                                                                            } else {
                                                                                                                                              var errs_1 = errors;
                                                                                                                                              if (typeof data['original-publisher'] !== "string") {
                                                                                                                                                MPBibliographyItem.errors = [{
                                                                                                                                                  keyword: 'type',
                                                                                                                                                  dataPath: (dataPath || '') + '[\'original-publisher\']',
                                                                                                                                                  schemaPath: '#/properties/original-publisher/type',
                                                                                                                                                  params: {
                                                                                                                                                    type: 'string'
                                                                                                                                                  },
                                                                                                                                                  message: 'should be string'
                                                                                                                                                }];
                                                                                                                                                return false;
                                                                                                                                              }
                                                                                                                                              var valid1 = errors === errs_1;
                                                                                                                                            }
                                                                                                                                            if (valid1) {
                                                                                                                                              if (data['original-publisher-place'] === undefined) {
                                                                                                                                                valid1 = true;
                                                                                                                                              } else {
                                                                                                                                                var errs_1 = errors;
                                                                                                                                                if (typeof data['original-publisher-place'] !== "string") {
                                                                                                                                                  MPBibliographyItem.errors = [{
                                                                                                                                                    keyword: 'type',
                                                                                                                                                    dataPath: (dataPath || '') + '[\'original-publisher-place\']',
                                                                                                                                                    schemaPath: '#/properties/original-publisher-place/type',
                                                                                                                                                    params: {
                                                                                                                                                      type: 'string'
                                                                                                                                                    },
                                                                                                                                                    message: 'should be string'
                                                                                                                                                  }];
                                                                                                                                                  return false;
                                                                                                                                                }
                                                                                                                                                var valid1 = errors === errs_1;
                                                                                                                                              }
                                                                                                                                              if (valid1) {
                                                                                                                                                var data1 = data['original-title'];
                                                                                                                                                if (data1 === undefined) {
                                                                                                                                                  valid1 = true;
                                                                                                                                                } else {
                                                                                                                                                  var errs_1 = errors;
                                                                                                                                                  if (typeof data1 !== "string" && !Array.isArray(data1)) {
                                                                                                                                                    MPBibliographyItem.errors = [{
                                                                                                                                                      keyword: 'type',
                                                                                                                                                      dataPath: (dataPath || '') + '[\'original-title\']',
                                                                                                                                                      schemaPath: '#/properties/original-title/type',
                                                                                                                                                      params: {
                                                                                                                                                        type: 'string,array'
                                                                                                                                                      },
                                                                                                                                                      message: 'should be string,array'
                                                                                                                                                    }];
                                                                                                                                                    return false;
                                                                                                                                                  }
                                                                                                                                                  var valid1 = errors === errs_1;
                                                                                                                                                }
                                                                                                                                                if (valid1) {
                                                                                                                                                  if (data.page === undefined) {
                                                                                                                                                    valid1 = true;
                                                                                                                                                  } else {
                                                                                                                                                    var errs_1 = errors;
                                                                                                                                                    if (typeof data.page !== "string") {
                                                                                                                                                      MPBibliographyItem.errors = [{
                                                                                                                                                        keyword: 'type',
                                                                                                                                                        dataPath: (dataPath || '') + '.page',
                                                                                                                                                        schemaPath: '#/properties/page/type',
                                                                                                                                                        params: {
                                                                                                                                                          type: 'string'
                                                                                                                                                        },
                                                                                                                                                        message: 'should be string'
                                                                                                                                                      }];
                                                                                                                                                      return false;
                                                                                                                                                    }
                                                                                                                                                    var valid1 = errors === errs_1;
                                                                                                                                                  }
                                                                                                                                                  if (valid1) {
                                                                                                                                                    if (data['page-first'] === undefined) {
                                                                                                                                                      valid1 = true;
                                                                                                                                                    } else {
                                                                                                                                                      var errs_1 = errors;
                                                                                                                                                      if (typeof data['page-first'] !== "string") {
                                                                                                                                                        MPBibliographyItem.errors = [{
                                                                                                                                                          keyword: 'type',
                                                                                                                                                          dataPath: (dataPath || '') + '[\'page-first\']',
                                                                                                                                                          schemaPath: '#/properties/page-first/type',
                                                                                                                                                          params: {
                                                                                                                                                            type: 'string'
                                                                                                                                                          },
                                                                                                                                                          message: 'should be string'
                                                                                                                                                        }];
                                                                                                                                                        return false;
                                                                                                                                                      }
                                                                                                                                                      var valid1 = errors === errs_1;
                                                                                                                                                    }
                                                                                                                                                    if (valid1) {
                                                                                                                                                      if (data.PMCID === undefined) {
                                                                                                                                                        valid1 = true;
                                                                                                                                                      } else {
                                                                                                                                                        var errs_1 = errors;
                                                                                                                                                        if (typeof data.PMCID !== "string") {
                                                                                                                                                          MPBibliographyItem.errors = [{
                                                                                                                                                            keyword: 'type',
                                                                                                                                                            dataPath: (dataPath || '') + '.PMCID',
                                                                                                                                                            schemaPath: '#/properties/PMCID/type',
                                                                                                                                                            params: {
                                                                                                                                                              type: 'string'
                                                                                                                                                            },
                                                                                                                                                            message: 'should be string'
                                                                                                                                                          }];
                                                                                                                                                          return false;
                                                                                                                                                        }
                                                                                                                                                        var valid1 = errors === errs_1;
                                                                                                                                                      }
                                                                                                                                                      if (valid1) {
                                                                                                                                                        if (data.PMID === undefined) {
                                                                                                                                                          valid1 = true;
                                                                                                                                                        } else {
                                                                                                                                                          var errs_1 = errors;
                                                                                                                                                          if (typeof data.PMID !== "string") {
                                                                                                                                                            MPBibliographyItem.errors = [{
                                                                                                                                                              keyword: 'type',
                                                                                                                                                              dataPath: (dataPath || '') + '.PMID',
                                                                                                                                                              schemaPath: '#/properties/PMID/type',
                                                                                                                                                              params: {
                                                                                                                                                                type: 'string'
                                                                                                                                                              },
                                                                                                                                                              message: 'should be string'
                                                                                                                                                            }];
                                                                                                                                                            return false;
                                                                                                                                                          }
                                                                                                                                                          var valid1 = errors === errs_1;
                                                                                                                                                        }
                                                                                                                                                        if (valid1) {
                                                                                                                                                          if (data.publisher === undefined) {
                                                                                                                                                            valid1 = true;
                                                                                                                                                          } else {
                                                                                                                                                            var errs_1 = errors;
                                                                                                                                                            if (typeof data.publisher !== "string") {
                                                                                                                                                              MPBibliographyItem.errors = [{
                                                                                                                                                                keyword: 'type',
                                                                                                                                                                dataPath: (dataPath || '') + '.publisher',
                                                                                                                                                                schemaPath: '#/properties/publisher/type',
                                                                                                                                                                params: {
                                                                                                                                                                  type: 'string'
                                                                                                                                                                },
                                                                                                                                                                message: 'should be string'
                                                                                                                                                              }];
                                                                                                                                                              return false;
                                                                                                                                                            }
                                                                                                                                                            var valid1 = errors === errs_1;
                                                                                                                                                          }
                                                                                                                                                          if (valid1) {
                                                                                                                                                            if (data['publisher-place'] === undefined) {
                                                                                                                                                              valid1 = true;
                                                                                                                                                            } else {
                                                                                                                                                              var errs_1 = errors;
                                                                                                                                                              if (typeof data['publisher-place'] !== "string") {
                                                                                                                                                                MPBibliographyItem.errors = [{
                                                                                                                                                                  keyword: 'type',
                                                                                                                                                                  dataPath: (dataPath || '') + '[\'publisher-place\']',
                                                                                                                                                                  schemaPath: '#/properties/publisher-place/type',
                                                                                                                                                                  params: {
                                                                                                                                                                    type: 'string'
                                                                                                                                                                  },
                                                                                                                                                                  message: 'should be string'
                                                                                                                                                                }];
                                                                                                                                                                return false;
                                                                                                                                                              }
                                                                                                                                                              var valid1 = errors === errs_1;
                                                                                                                                                            }
                                                                                                                                                            if (valid1) {
                                                                                                                                                              if (data.references === undefined) {
                                                                                                                                                                valid1 = true;
                                                                                                                                                              } else {
                                                                                                                                                                var errs_1 = errors;
                                                                                                                                                                if (typeof data.references !== "string") {
                                                                                                                                                                  MPBibliographyItem.errors = [{
                                                                                                                                                                    keyword: 'type',
                                                                                                                                                                    dataPath: (dataPath || '') + '.references',
                                                                                                                                                                    schemaPath: '#/properties/references/type',
                                                                                                                                                                    params: {
                                                                                                                                                                      type: 'string'
                                                                                                                                                                    },
                                                                                                                                                                    message: 'should be string'
                                                                                                                                                                  }];
                                                                                                                                                                  return false;
                                                                                                                                                                }
                                                                                                                                                                var valid1 = errors === errs_1;
                                                                                                                                                              }
                                                                                                                                                              if (valid1) {
                                                                                                                                                                if (data['reviewed-title'] === undefined) {
                                                                                                                                                                  valid1 = true;
                                                                                                                                                                } else {
                                                                                                                                                                  var errs_1 = errors;
                                                                                                                                                                  if (typeof data['reviewed-title'] !== "string") {
                                                                                                                                                                    MPBibliographyItem.errors = [{
                                                                                                                                                                      keyword: 'type',
                                                                                                                                                                      dataPath: (dataPath || '') + '[\'reviewed-title\']',
                                                                                                                                                                      schemaPath: '#/properties/reviewed-title/type',
                                                                                                                                                                      params: {
                                                                                                                                                                        type: 'string'
                                                                                                                                                                      },
                                                                                                                                                                      message: 'should be string'
                                                                                                                                                                    }];
                                                                                                                                                                    return false;
                                                                                                                                                                  }
                                                                                                                                                                  var valid1 = errors === errs_1;
                                                                                                                                                                }
                                                                                                                                                                if (valid1) {
                                                                                                                                                                  if (data.scale === undefined) {
                                                                                                                                                                    valid1 = true;
                                                                                                                                                                  } else {
                                                                                                                                                                    var errs_1 = errors;
                                                                                                                                                                    if (typeof data.scale !== "string") {
                                                                                                                                                                      MPBibliographyItem.errors = [{
                                                                                                                                                                        keyword: 'type',
                                                                                                                                                                        dataPath: (dataPath || '') + '.scale',
                                                                                                                                                                        schemaPath: '#/properties/scale/type',
                                                                                                                                                                        params: {
                                                                                                                                                                          type: 'string'
                                                                                                                                                                        },
                                                                                                                                                                        message: 'should be string'
                                                                                                                                                                      }];
                                                                                                                                                                      return false;
                                                                                                                                                                    }
                                                                                                                                                                    var valid1 = errors === errs_1;
                                                                                                                                                                  }
                                                                                                                                                                  if (valid1) {
                                                                                                                                                                    if (data.section === undefined) {
                                                                                                                                                                      valid1 = true;
                                                                                                                                                                    } else {
                                                                                                                                                                      var errs_1 = errors;
                                                                                                                                                                      if (typeof data.section !== "string") {
                                                                                                                                                                        MPBibliographyItem.errors = [{
                                                                                                                                                                          keyword: 'type',
                                                                                                                                                                          dataPath: (dataPath || '') + '.section',
                                                                                                                                                                          schemaPath: '#/properties/section/type',
                                                                                                                                                                          params: {
                                                                                                                                                                            type: 'string'
                                                                                                                                                                          },
                                                                                                                                                                          message: 'should be string'
                                                                                                                                                                        }];
                                                                                                                                                                        return false;
                                                                                                                                                                      }
                                                                                                                                                                      var valid1 = errors === errs_1;
                                                                                                                                                                    }
                                                                                                                                                                    if (valid1) {
                                                                                                                                                                      if (data.source === undefined) {
                                                                                                                                                                        valid1 = true;
                                                                                                                                                                      } else {
                                                                                                                                                                        var errs_1 = errors;
                                                                                                                                                                        if (typeof data.source !== "string") {
                                                                                                                                                                          MPBibliographyItem.errors = [{
                                                                                                                                                                            keyword: 'type',
                                                                                                                                                                            dataPath: (dataPath || '') + '.source',
                                                                                                                                                                            schemaPath: '#/properties/source/type',
                                                                                                                                                                            params: {
                                                                                                                                                                              type: 'string'
                                                                                                                                                                            },
                                                                                                                                                                            message: 'should be string'
                                                                                                                                                                          }];
                                                                                                                                                                          return false;
                                                                                                                                                                        }
                                                                                                                                                                        var valid1 = errors === errs_1;
                                                                                                                                                                      }
                                                                                                                                                                      if (valid1) {
                                                                                                                                                                        if (data.status === undefined) {
                                                                                                                                                                          valid1 = true;
                                                                                                                                                                        } else {
                                                                                                                                                                          var errs_1 = errors;
                                                                                                                                                                          if (typeof data.status !== "string") {
                                                                                                                                                                            MPBibliographyItem.errors = [{
                                                                                                                                                                              keyword: 'type',
                                                                                                                                                                              dataPath: (dataPath || '') + '.status',
                                                                                                                                                                              schemaPath: '#/properties/status/type',
                                                                                                                                                                              params: {
                                                                                                                                                                                type: 'string'
                                                                                                                                                                              },
                                                                                                                                                                              message: 'should be string'
                                                                                                                                                                            }];
                                                                                                                                                                            return false;
                                                                                                                                                                          }
                                                                                                                                                                          var valid1 = errors === errs_1;
                                                                                                                                                                        }
                                                                                                                                                                        if (valid1) {
                                                                                                                                                                          if (data.title === undefined) {
                                                                                                                                                                            valid1 = true;
                                                                                                                                                                          } else {
                                                                                                                                                                            var errs_1 = errors;
                                                                                                                                                                            if (typeof data.title !== "string") {
                                                                                                                                                                              MPBibliographyItem.errors = [{
                                                                                                                                                                                keyword: 'type',
                                                                                                                                                                                dataPath: (dataPath || '') + '.title',
                                                                                                                                                                                schemaPath: '#/properties/title/type',
                                                                                                                                                                                params: {
                                                                                                                                                                                  type: 'string'
                                                                                                                                                                                },
                                                                                                                                                                                message: 'should be string'
                                                                                                                                                                              }];
                                                                                                                                                                              return false;
                                                                                                                                                                            }
                                                                                                                                                                            var valid1 = errors === errs_1;
                                                                                                                                                                          }
                                                                                                                                                                          if (valid1) {
                                                                                                                                                                            if (data['title-short'] === undefined) {
                                                                                                                                                                              valid1 = true;
                                                                                                                                                                            } else {
                                                                                                                                                                              var errs_1 = errors;
                                                                                                                                                                              if (typeof data['title-short'] !== "string") {
                                                                                                                                                                                MPBibliographyItem.errors = [{
                                                                                                                                                                                  keyword: 'type',
                                                                                                                                                                                  dataPath: (dataPath || '') + '[\'title-short\']',
                                                                                                                                                                                  schemaPath: '#/properties/title-short/type',
                                                                                                                                                                                  params: {
                                                                                                                                                                                    type: 'string'
                                                                                                                                                                                  },
                                                                                                                                                                                  message: 'should be string'
                                                                                                                                                                                }];
                                                                                                                                                                                return false;
                                                                                                                                                                              }
                                                                                                                                                                              var valid1 = errors === errs_1;
                                                                                                                                                                            }
                                                                                                                                                                            if (valid1) {
                                                                                                                                                                              if (data.URL === undefined) {
                                                                                                                                                                                valid1 = true;
                                                                                                                                                                              } else {
                                                                                                                                                                                var errs_1 = errors;
                                                                                                                                                                                if (typeof data.URL !== "string") {
                                                                                                                                                                                  MPBibliographyItem.errors = [{
                                                                                                                                                                                    keyword: 'type',
                                                                                                                                                                                    dataPath: (dataPath || '') + '.URL',
                                                                                                                                                                                    schemaPath: '#/properties/URL/type',
                                                                                                                                                                                    params: {
                                                                                                                                                                                      type: 'string'
                                                                                                                                                                                    },
                                                                                                                                                                                    message: 'should be string'
                                                                                                                                                                                  }];
                                                                                                                                                                                  return false;
                                                                                                                                                                                }
                                                                                                                                                                                var valid1 = errors === errs_1;
                                                                                                                                                                              }
                                                                                                                                                                              if (valid1) {
                                                                                                                                                                                if (data.version === undefined) {
                                                                                                                                                                                  valid1 = true;
                                                                                                                                                                                } else {
                                                                                                                                                                                  var errs_1 = errors;
                                                                                                                                                                                  if (typeof data.version !== "string") {
                                                                                                                                                                                    MPBibliographyItem.errors = [{
                                                                                                                                                                                      keyword: 'type',
                                                                                                                                                                                      dataPath: (dataPath || '') + '.version',
                                                                                                                                                                                      schemaPath: '#/properties/version/type',
                                                                                                                                                                                      params: {
                                                                                                                                                                                        type: 'string'
                                                                                                                                                                                      },
                                                                                                                                                                                      message: 'should be string'
                                                                                                                                                                                    }];
                                                                                                                                                                                    return false;
                                                                                                                                                                                  }
                                                                                                                                                                                  var valid1 = errors === errs_1;
                                                                                                                                                                                }
                                                                                                                                                                                if (valid1) {
                                                                                                                                                                                  if (data.volume === undefined) {
                                                                                                                                                                                    valid1 = true;
                                                                                                                                                                                  } else {
                                                                                                                                                                                    var errs_1 = errors;
                                                                                                                                                                                    if (typeof data.volume !== "string") {
                                                                                                                                                                                      MPBibliographyItem.errors = [{
                                                                                                                                                                                        keyword: 'type',
                                                                                                                                                                                        dataPath: (dataPath || '') + '.volume',
                                                                                                                                                                                        schemaPath: '#/properties/volume/type',
                                                                                                                                                                                        params: {
                                                                                                                                                                                          type: 'string'
                                                                                                                                                                                        },
                                                                                                                                                                                        message: 'should be string'
                                                                                                                                                                                      }];
                                                                                                                                                                                      return false;
                                                                                                                                                                                    }
                                                                                                                                                                                    var valid1 = errors === errs_1;
                                                                                                                                                                                  }
                                                                                                                                                                                  if (valid1) {
                                                                                                                                                                                    if (data['year-suffix'] === undefined) {
                                                                                                                                                                                      valid1 = true;
                                                                                                                                                                                    } else {
                                                                                                                                                                                      var errs_1 = errors;
                                                                                                                                                                                      if (typeof data['year-suffix'] !== "string") {
                                                                                                                                                                                        MPBibliographyItem.errors = [{
                                                                                                                                                                                          keyword: 'type',
                                                                                                                                                                                          dataPath: (dataPath || '') + '[\'year-suffix\']',
                                                                                                                                                                                          schemaPath: '#/properties/year-suffix/type',
                                                                                                                                                                                          params: {
                                                                                                                                                                                            type: 'string'
                                                                                                                                                                                          },
                                                                                                                                                                                          message: 'should be string'
                                                                                                                                                                                        }];
                                                                                                                                                                                        return false;
                                                                                                                                                                                      }
                                                                                                                                                                                      var valid1 = errors === errs_1;
                                                                                                                                                                                    }
                                                                                                                                                                                    if (valid1) {
                                                                                                                                                                                      var data1 = data.keywordIDs;
                                                                                                                                                                                      if (data1 === undefined) {
                                                                                                                                                                                        valid1 = true;
                                                                                                                                                                                      } else {
                                                                                                                                                                                        var errs_1 = errors;
                                                                                                                                                                                        if (Array.isArray(data1)) {
                                                                                                                                                                                          var errs__1 = errors;
                                                                                                                                                                                          var valid1;
                                                                                                                                                                                          for (var i1 = 0; i1 < data1.length; i1++) {
                                                                                                                                                                                            var data2 = data1[i1];
                                                                                                                                                                                            var errs_2 = errors;
                                                                                                                                                                                            var errs_3 = errors;
                                                                                                                                                                                            if (typeof data2 === "string") {
                                                                                                                                                                                              if (!pattern3.test(data2)) {
                                                                                                                                                                                                MPBibliographyItem.errors = [{
                                                                                                                                                                                                  keyword: 'pattern',
                                                                                                                                                                                                  dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                                                                                                                                                                                  schemaPath: 'strings.json#/definitions/keywordId/pattern',
                                                                                                                                                                                                  params: {
                                                                                                                                                                                                    pattern: '^MPKeyword:[0-9a-zA-Z\\-]+'
                                                                                                                                                                                                  },
                                                                                                                                                                                                  message: 'should match pattern "^MPKeyword:[0-9a-zA-Z\\-]+"'
                                                                                                                                                                                                }];
                                                                                                                                                                                                return false;
                                                                                                                                                                                              }
                                                                                                                                                                                            } else {
                                                                                                                                                                                              MPBibliographyItem.errors = [{
                                                                                                                                                                                                keyword: 'type',
                                                                                                                                                                                                dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                                                                                                                                                                                schemaPath: 'strings.json#/definitions/keywordId/type',
                                                                                                                                                                                                params: {
                                                                                                                                                                                                  type: 'string'
                                                                                                                                                                                                },
                                                                                                                                                                                                message: 'should be string'
                                                                                                                                                                                              }];
                                                                                                                                                                                              return false;
                                                                                                                                                                                            }
                                                                                                                                                                                            var valid3 = errors === errs_3;
                                                                                                                                                                                            var valid2 = errors === errs_2;
                                                                                                                                                                                            if (!valid2) break;
                                                                                                                                                                                          }
                                                                                                                                                                                        } else {
                                                                                                                                                                                          MPBibliographyItem.errors = [{
                                                                                                                                                                                            keyword: 'type',
                                                                                                                                                                                            dataPath: (dataPath || '') + '.keywordIDs',
                                                                                                                                                                                            schemaPath: '#/properties/keywordIDs/type',
                                                                                                                                                                                            params: {
                                                                                                                                                                                              type: 'array'
                                                                                                                                                                                            },
                                                                                                                                                                                            message: 'should be array'
                                                                                                                                                                                          }];
                                                                                                                                                                                          return false;
                                                                                                                                                                                        }
                                                                                                                                                                                        var valid1 = errors === errs_1;
                                                                                                                                                                                      }
                                                                                                                                                                                    }
                                                                                                                                                                                  }
                                                                                                                                                                                }
                                                                                                                                                                              }
                                                                                                                                                                            }
                                                                                                                                                                          }
                                                                                                                                                                        }
                                                                                                                                                                      }
                                                                                                                                                                    }
                                                                                                                                                                  }
                                                                                                                                                                }
                                                                                                                                                              }
                                                                                                                                                            }
                                                                                                                                                          }
                                                                                                                                                        }
                                                                                                                                                      }
                                                                                                                                                    }
                                                                                                                                                  }
                                                                                                                                                }
                                                                                                                                              }
                                                                                                                                            }
                                                                                                                                          }
                                                                                                                                        }
                                                                                                                                      }
                                                                                                                                    }
                                                                                                                                  }
                                                                                                                                }
                                                                                                                              }
                                                                                                                            }
                                                                                                                          }
                                                                                                                        }
                                                                                                                      }
                                                                                                                    }
                                                                                                                  }
                                                                                                                }
                                                                                                              }
                                                                                                            }
                                                                                                          }
                                                                                                        }
                                                                                                      }
                                                                                                    }
                                                                                                  }
                                                                                                }
                                                                                              }
                                                                                            }
                                                                                          }
                                                                                        }
                                                                                      }
                                                                                    }
                                                                                  }
                                                                                }
                                                                              }
                                                                            }
                                                                          }
                                                                        }
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPBibliographyItem.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPBibliographyItem.errors = vErrors;
      return errors === 0;
    };
    MPBibliographyItem.schema = {
      "$id": "MPBibliographyItem.json",
      "additionalProperties": false,
      "properties": {
        "library_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?Library"
          }]
        },
        "_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?BibliographyItem"
          }]
        },
        "_rev": {
          "type": "string"
        },
        "favorited": {
          "type": "boolean"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "objectType": {
          "type": "string",
          "enum": ["BibliographyItem", "MPBibliographyItem"]
        },
        "type": {
          "type": "string",
          "enum": ["article", "article-journal", "article-magazine", "article-newspaper", "bill", "book", "broadcast", "chapter", "dataset", "entry", "entry-dictionary", "entry-encyclopedia", "figure", "graphic", "interview", "legal_case", "legislation", "manuscript", "map", "motion_picture", "musical_score", "pamphlet", "paper-conference", "patent", "personal_communication", "post", "post-weblog", "report", "review", "review-book", "song", "speech", "thesis", "treaty", "webpage"]
        },
        "categories": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "language": {
          "type": "string"
        },
        "journalAbbreviation": {
          "type": "string"
        },
        "shortTitle": {
          "type": "string"
        },
        "author": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          },
          "additionalItems": false
        },
        "collection-editor": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "composer": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "container-author": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "director": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "editor": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "editorial-director": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "interviewer": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "illustrator": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "original-author": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "recipient": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "reviewed-author": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "translator": {
          "type": "array",
          "items": {
            "$ref": "MPBibliographicName.json#"
          }
        },
        "accessed": {
          "$ref": "MPBibliographicDate.json#"
        },
        "container": {
          "$ref": "MPBibliographicDate.json#"
        },
        "event-date": {
          "$ref": "MPBibliographicDate.json#"
        },
        "issued": {
          "$ref": "MPBibliographicDate.json#"
        },
        "original-date": {
          "$ref": "MPBibliographicDate.json#"
        },
        "submitted": {
          "$ref": "MPBibliographicDate.json#"
        },
        "abstract": {
          "type": "string"
        },
        "annote": {
          "type": "string"
        },
        "archive": {
          "type": "string"
        },
        "archive_location": {
          "type": "string"
        },
        "archive-place": {
          "type": "string"
        },
        "authority": {
          "type": "string"
        },
        "call-number": {
          "type": "string"
        },
        "chapter-number": {
          "type": "integer"
        },
        "citation-number": {
          "type": "integer"
        },
        "citation-label": {
          "type": "string"
        },
        "collection-number": {
          "type": "integer"
        },
        "collection-title": {
          "type": "string"
        },
        "container-title": {
          "type": "string"
        },
        "container-title-short": {
          "type": "string"
        },
        "dimensions": {
          "type": "string"
        },
        "DOI": {
          "type": "string"
        },
        "edition": {
          "type": "integer"
        },
        "event": {
          "type": "string"
        },
        "event-place": {
          "type": "string"
        },
        "first-reference-note-number": {
          "type": "string"
        },
        "genre": {
          "type": "string"
        },
        "ISBN": {
          "type": "string"
        },
        "ISSN": {
          "type": ["string", "array"]
        },
        "issue": {
          "type": ["string", "number"]
        },
        "jurisdiction": {
          "type": "string"
        },
        "keyword": {
          "type": "string"
        },
        "locator": {
          "type": "string"
        },
        "medium": {
          "type": "string"
        },
        "note": {
          "type": "string"
        },
        "number": {
          "type": "integer"
        },
        "number-of-pages": {
          "type": "integer"
        },
        "number-of-volumes": {
          "type": "integer"
        },
        "original-publisher": {
          "type": "string"
        },
        "original-publisher-place": {
          "type": "string"
        },
        "original-title": {
          "type": ["string", "array"]
        },
        "page": {
          "type": "string"
        },
        "page-first": {
          "type": "string"
        },
        "PMCID": {
          "type": "string"
        },
        "PMID": {
          "type": "string"
        },
        "publisher": {
          "type": "string"
        },
        "publisher-place": {
          "type": "string"
        },
        "references": {
          "type": "string"
        },
        "reviewed-title": {
          "type": "string"
        },
        "scale": {
          "type": "string"
        },
        "section": {
          "type": "string"
        },
        "source": {
          "type": "string"
        },
        "status": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "title-short": {
          "type": "string"
        },
        "URL": {
          "type": "string"
        },
        "version": {
          "type": "string"
        },
        "volume": {
          "type": "string"
        },
        "year-suffix": {
          "type": "string"
        },
        "keywordIDs": {
          "type": "array",
          "items": {
            "$ref": "strings.json#/definitions/keywordId"
          }
        }
      },
      "required": ["library_id", "_id", "objectType", "type"],
      "type": "object"
    };
    MPBibliographyItem.errors = null;

    function MPFundingBody(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?Manuscript');
      var pattern2 = new RegExp('^(MP)?Project');
      var pattern3 = new RegExp('^(MP)?Library');
      var pattern4 = new RegExp('^MPKeyword:[0-9a-zA-Z\\-]+');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      };
      var refVal3 = {
        "type": "string",
        "pattern": "^MPKeyword:[0-9a-zA-Z\\-]+"
      }; /*# sourceURL=MPFundingBody.json */
      var vErrors = null;
      var errors = 0;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || MPFundingBody.schema.properties.hasOwnProperty(key0));
            if (isAdditional0) {
              valid1 = false;
              MPFundingBody.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data.manuscript;
            if (data1 === undefined) {
              valid1 = false;
              MPFundingBody.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: 'manuscript'
                },
                message: 'should have required property \'manuscript\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPFundingBody.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '.manuscript',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPFundingBody.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '.manuscript',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPFundingBody.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.manuscript',
                      schemaPath: '#/properties/manuscript/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?Manuscript'
                      },
                      message: 'should match pattern "^(MP)?Manuscript"'
                    }];
                    return false;
                  }
                } else {
                  MPFundingBody.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.manuscript',
                    schemaPath: '#/properties/manuscript/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              var data1 = data.container_id;
              if (data1 === undefined) {
                valid1 = false;
                MPFundingBody.errors = [{
                  keyword: 'required',
                  dataPath: (dataPath || '') + "",
                  schemaPath: '#/required',
                  params: {
                    missingProperty: 'container_id'
                  },
                  message: 'should have required property \'container_id\''
                }];
                return false;
              } else {
                var errs_1 = errors;
                var errs_2 = errors;
                var errs_3 = errors;
                if (typeof data1 === "string") {
                  if (!pattern0.test(data1)) {
                    MPFundingBody.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: 'strings.json#/definitions/_id/pattern',
                      params: {
                        pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                      },
                      message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                    }];
                    return false;
                  }
                } else {
                  MPFundingBody.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.container_id',
                    schemaPath: 'strings.json#/definitions/_id/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid3 = errors === errs_3;
                var valid2 = errors === errs_2;
                if (valid2) {
                  var errs_2 = errors;
                  var errs__2 = errors;
                  var valid2 = false;
                  var errs_3 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern2.test(data1)) {
                      var err = {
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '.container_id',
                        schemaPath: '#/properties/container_id/allOf/1/anyOf/0/pattern',
                        params: {
                          pattern: '^(MP)?Project'
                        },
                        message: 'should match pattern "^(MP)?Project"'
                      };
                      if (vErrors === null) vErrors = [err];
                      else vErrors.push(err);
                      errors++;
                    }
                  } else {
                    var err = {
                      keyword: 'type',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: '#/properties/container_id/allOf/1/anyOf/0/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    };
                    if (vErrors === null) vErrors = [err];
                    else vErrors.push(err);
                    errors++;
                  }
                  var valid3 = errors === errs_3;
                  valid2 = valid2 || valid3;
                  if (!valid2) {
                    var errs_3 = errors;
                    if (typeof data1 === "string") {
                      if (!pattern3.test(data1)) {
                        var err = {
                          keyword: 'pattern',
                          dataPath: (dataPath || '') + '.container_id',
                          schemaPath: '#/properties/container_id/allOf/1/anyOf/1/pattern',
                          params: {
                            pattern: '^(MP)?Library'
                          },
                          message: 'should match pattern "^(MP)?Library"'
                        };
                        if (vErrors === null) vErrors = [err];
                        else vErrors.push(err);
                        errors++;
                      }
                    } else {
                      var err = {
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.container_id',
                        schemaPath: '#/properties/container_id/allOf/1/anyOf/1/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      };
                      if (vErrors === null) vErrors = [err];
                      else vErrors.push(err);
                      errors++;
                    }
                    var valid3 = errors === errs_3;
                    valid2 = valid2 || valid3;
                  }
                  if (!valid2) {
                    var err = {
                      keyword: 'anyOf',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: '#/properties/container_id/allOf/1/anyOf',
                      params: {},
                      message: 'should match some schema in anyOf'
                    };
                    if (vErrors === null) vErrors = [err];
                    else vErrors.push(err);
                    errors++;
                    MPFundingBody.errors = vErrors;
                    return false;
                  } else {
                    errors = errs__2;
                    if (vErrors !== null) {
                      if (errs__2) vErrors.length = errs__2;
                      else vErrors = null;
                    }
                  }
                  var valid2 = errors === errs_2;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data._id;
                if (data1 === undefined) {
                  valid1 = false;
                  MPFundingBody.errors = [{
                    keyword: 'required',
                    dataPath: (dataPath || '') + "",
                    schemaPath: '#/required',
                    params: {
                      missingProperty: '_id'
                    },
                    message: 'should have required property \'_id\''
                  }];
                  return false;
                } else {
                  var errs_1 = errors;
                  var errs_2 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern0.test(data1)) {
                      MPFundingBody.errors = [{
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '._id',
                        schemaPath: 'strings.json#/definitions/_id/pattern',
                        params: {
                          pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                        },
                        message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                      }];
                      return false;
                    }
                  } else {
                    MPFundingBody.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: 'strings.json#/definitions/_id/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    }];
                    return false;
                  }
                  var valid2 = errors === errs_2;
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  if (data._rev === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    if (typeof data._rev !== "string") {
                      MPFundingBody.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '._rev',
                        schemaPath: '#/properties/_rev/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data._revisions;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                        var errs__1 = errors;
                        var valid2 = true;
                        var data2 = data1.start;
                        if (data2 === undefined) {
                          valid2 = true;
                        } else {
                          var errs_2 = errors;
                          if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                            MPFundingBody.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '._revisions.start',
                              schemaPath: '#/properties/_revisions/properties/start/type',
                              params: {
                                type: 'integer'
                              },
                              message: 'should be integer'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                        }
                        if (valid2) {
                          var data2 = data1.ids;
                          if (data2 === undefined) {
                            valid2 = true;
                          } else {
                            var errs_2 = errors;
                            if (Array.isArray(data2)) {
                              var errs__2 = errors;
                              var valid2;
                              for (var i2 = 0; i2 < data2.length; i2++) {
                                var errs_3 = errors;
                                if (typeof data2[i2] !== "string") {
                                  MPFundingBody.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                    schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid3 = errors === errs_3;
                                if (!valid3) break;
                              }
                            } else {
                              MPFundingBody.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids',
                                schemaPath: '#/properties/_revisions/properties/ids/type',
                                params: {
                                  type: 'array'
                                },
                                message: 'should be array'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                          }
                        }
                      } else {
                        MPFundingBody.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions',
                          schemaPath: '#/properties/_revisions/type',
                          params: {
                            type: 'object'
                          },
                          message: 'should be object'
                        }];
                        return false;
                      }
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      if (data.sessionID === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        if (typeof data.sessionID !== "string") {
                          MPFundingBody.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.sessionID',
                            schemaPath: '#/properties/sessionID/type',
                            params: {
                              type: 'string'
                            },
                            message: 'should be string'
                          }];
                          return false;
                        }
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data.createdAt;
                        if (data1 === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          var errs_2 = errors;
                          if (typeof data1 === "number") {
                            if (data1 > 2000000000 || data1 !== data1) {
                              MPFundingBody.errors = [{
                                keyword: 'maximum',
                                dataPath: (dataPath || '') + '.createdAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                params: {
                                  comparison: '<=',
                                  limit: 2000000000,
                                  exclusive: false
                                },
                                message: 'should be <= 2000000000'
                              }];
                              return false;
                            } else {
                              if (data1 < 0 || data1 !== data1) {
                                MPFundingBody.errors = [{
                                  keyword: 'minimum',
                                  dataPath: (dataPath || '') + '.createdAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                  params: {
                                    comparison: '>=',
                                    limit: 0,
                                    exclusive: false
                                  },
                                  message: 'should be >= 0'
                                }];
                                return false;
                              }
                            }
                          } else {
                            MPFundingBody.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.createdAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/type',
                              params: {
                                type: 'number'
                              },
                              message: 'should be number'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          var data1 = data.updatedAt;
                          if (data1 === undefined) {
                            valid1 = true;
                          } else {
                            var errs_1 = errors;
                            var errs_2 = errors;
                            if (typeof data1 === "number") {
                              if (data1 > 2000000000 || data1 !== data1) {
                                MPFundingBody.errors = [{
                                  keyword: 'maximum',
                                  dataPath: (dataPath || '') + '.updatedAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                  params: {
                                    comparison: '<=',
                                    limit: 2000000000,
                                    exclusive: false
                                  },
                                  message: 'should be <= 2000000000'
                                }];
                                return false;
                              } else {
                                if (data1 < 0 || data1 !== data1) {
                                  MPFundingBody.errors = [{
                                    keyword: 'minimum',
                                    dataPath: (dataPath || '') + '.updatedAt',
                                    schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                    params: {
                                      comparison: '>=',
                                      limit: 0,
                                      exclusive: false
                                    },
                                    message: 'should be >= 0'
                                  }];
                                  return false;
                                }
                              }
                            } else {
                              MPFundingBody.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.updatedAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/type',
                                params: {
                                  type: 'number'
                                },
                                message: 'should be number'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            var data1 = data.keywordIDs;
                            if (data1 === undefined) {
                              valid1 = true;
                            } else {
                              var errs_1 = errors;
                              if (Array.isArray(data1)) {
                                var errs__1 = errors;
                                var valid1;
                                for (var i1 = 0; i1 < data1.length; i1++) {
                                  var data2 = data1[i1];
                                  var errs_2 = errors;
                                  var errs_3 = errors;
                                  if (typeof data2 === "string") {
                                    if (!pattern4.test(data2)) {
                                      MPFundingBody.errors = [{
                                        keyword: 'pattern',
                                        dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                        schemaPath: 'strings.json#/definitions/keywordId/pattern',
                                        params: {
                                          pattern: '^MPKeyword:[0-9a-zA-Z\\-]+'
                                        },
                                        message: 'should match pattern "^MPKeyword:[0-9a-zA-Z\\-]+"'
                                      }];
                                      return false;
                                    }
                                  } else {
                                    MPFundingBody.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                      schemaPath: 'strings.json#/definitions/keywordId/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid3 = errors === errs_3;
                                  var valid2 = errors === errs_2;
                                  if (!valid2) break;
                                }
                              } else {
                                MPFundingBody.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.keywordIDs',
                                  schemaPath: '#/properties/keywordIDs/type',
                                  params: {
                                    type: 'array'
                                  },
                                  message: 'should be array'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              if (data.name === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if (typeof data.name !== "string") {
                                  MPFundingBody.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.name',
                                    schemaPath: '#/properties/name/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.URL === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.URL !== "string") {
                                    MPFundingBody.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.URL',
                                      schemaPath: '#/properties/URL/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                                if (valid1) {
                                  var data1 = data.objectType;
                                  if (data1 === undefined) {
                                    valid1 = false;
                                    MPFundingBody.errors = [{
                                      keyword: 'required',
                                      dataPath: (dataPath || '') + "",
                                      schemaPath: '#/required',
                                      params: {
                                        missingProperty: 'objectType'
                                      },
                                      message: 'should have required property \'objectType\''
                                    }];
                                    return false;
                                  } else {
                                    var errs_1 = errors;
                                    if (typeof data1 !== "string") {
                                      MPFundingBody.errors = [{
                                        keyword: 'type',
                                        dataPath: (dataPath || '') + '.objectType',
                                        schemaPath: '#/properties/objectType/type',
                                        params: {
                                          type: 'string'
                                        },
                                        message: 'should be string'
                                      }];
                                      return false;
                                    }
                                    var schema1 = MPFundingBody.schema.properties.objectType.enum;
                                    var valid1;
                                    valid1 = false;
                                    for (var i1 = 0; i1 < schema1.length; i1++)
                                      if (equal(data1, schema1[i1])) {
                                        valid1 = true;
                                        break;
                                      }
                                    if (!valid1) {
                                      MPFundingBody.errors = [{
                                        keyword: 'enum',
                                        dataPath: (dataPath || '') + '.objectType',
                                        schemaPath: '#/properties/objectType/enum',
                                        params: {
                                          allowedValues: schema1
                                        },
                                        message: 'should be equal to one of the allowed values'
                                      }];
                                      return false;
                                    }
                                    var valid1 = errors === errs_1;
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPFundingBody.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPFundingBody.errors = vErrors;
      return errors === 0;
    };
    MPFundingBody.schema = {
      "$id": "MPFundingBody.json",
      "additionalProperties": false,
      "properties": {
        "manuscript": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?Manuscript"
          }]
        },
        "container_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "anyOf": [{
              "type": "string",
              "pattern": "^(MP)?Project"
            }, {
              "type": "string",
              "pattern": "^(MP)?Library"
            }]
          }]
        },
        "_id": {
          "$ref": "strings.json#/definitions/_id"
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "sessionID": {
          "type": "string"
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "keywordIDs": {
          "type": "array",
          "items": {
            "$ref": "strings.json#/definitions/keywordId"
          }
        },
        "name": {
          "type": "string"
        },
        "URL": {
          "type": "string"
        },
        "objectType": {
          "type": "string",
          "enum": ["FundingBody", "MPFundingBody"]
        }
      },
      "required": ["manuscript", "container_id", "_id", "objectType"],
      "title": "FundingBody",
      "type": "object"
    };
    MPFundingBody.errors = null;

    function MPGrant(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?Manuscript');
      var pattern2 = new RegExp('^(MP)?Project');
      var pattern3 = new RegExp('^(MP)?Library');
      var pattern4 = new RegExp('^MPKeyword:[0-9a-zA-Z\\-]+');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      };
      var refVal3 = {
        "type": "string",
        "pattern": "^MPKeyword:[0-9a-zA-Z\\-]+"
      };
      var refVal4 = MPFundingBody; /*# sourceURL=MPGrant.json */
      var vErrors = null;
      var errors = 0;
      if (rootData === undefined) rootData = data;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || MPGrant.schema.properties.hasOwnProperty(key0));
            if (isAdditional0) {
              valid1 = false;
              MPGrant.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data.manuscript;
            if (data1 === undefined) {
              valid1 = false;
              MPGrant.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: 'manuscript'
                },
                message: 'should have required property \'manuscript\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPGrant.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '.manuscript',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPGrant.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '.manuscript',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPGrant.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.manuscript',
                      schemaPath: '#/properties/manuscript/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?Manuscript'
                      },
                      message: 'should match pattern "^(MP)?Manuscript"'
                    }];
                    return false;
                  }
                } else {
                  MPGrant.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.manuscript',
                    schemaPath: '#/properties/manuscript/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              var data1 = data.container_id;
              if (data1 === undefined) {
                valid1 = false;
                MPGrant.errors = [{
                  keyword: 'required',
                  dataPath: (dataPath || '') + "",
                  schemaPath: '#/required',
                  params: {
                    missingProperty: 'container_id'
                  },
                  message: 'should have required property \'container_id\''
                }];
                return false;
              } else {
                var errs_1 = errors;
                var errs_2 = errors;
                var errs_3 = errors;
                if (typeof data1 === "string") {
                  if (!pattern0.test(data1)) {
                    MPGrant.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: 'strings.json#/definitions/_id/pattern',
                      params: {
                        pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                      },
                      message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                    }];
                    return false;
                  }
                } else {
                  MPGrant.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.container_id',
                    schemaPath: 'strings.json#/definitions/_id/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid3 = errors === errs_3;
                var valid2 = errors === errs_2;
                if (valid2) {
                  var errs_2 = errors;
                  var errs__2 = errors;
                  var valid2 = false;
                  var errs_3 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern2.test(data1)) {
                      var err = {
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '.container_id',
                        schemaPath: '#/properties/container_id/allOf/1/anyOf/0/pattern',
                        params: {
                          pattern: '^(MP)?Project'
                        },
                        message: 'should match pattern "^(MP)?Project"'
                      };
                      if (vErrors === null) vErrors = [err];
                      else vErrors.push(err);
                      errors++;
                    }
                  } else {
                    var err = {
                      keyword: 'type',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: '#/properties/container_id/allOf/1/anyOf/0/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    };
                    if (vErrors === null) vErrors = [err];
                    else vErrors.push(err);
                    errors++;
                  }
                  var valid3 = errors === errs_3;
                  valid2 = valid2 || valid3;
                  if (!valid2) {
                    var errs_3 = errors;
                    if (typeof data1 === "string") {
                      if (!pattern3.test(data1)) {
                        var err = {
                          keyword: 'pattern',
                          dataPath: (dataPath || '') + '.container_id',
                          schemaPath: '#/properties/container_id/allOf/1/anyOf/1/pattern',
                          params: {
                            pattern: '^(MP)?Library'
                          },
                          message: 'should match pattern "^(MP)?Library"'
                        };
                        if (vErrors === null) vErrors = [err];
                        else vErrors.push(err);
                        errors++;
                      }
                    } else {
                      var err = {
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.container_id',
                        schemaPath: '#/properties/container_id/allOf/1/anyOf/1/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      };
                      if (vErrors === null) vErrors = [err];
                      else vErrors.push(err);
                      errors++;
                    }
                    var valid3 = errors === errs_3;
                    valid2 = valid2 || valid3;
                  }
                  if (!valid2) {
                    var err = {
                      keyword: 'anyOf',
                      dataPath: (dataPath || '') + '.container_id',
                      schemaPath: '#/properties/container_id/allOf/1/anyOf',
                      params: {},
                      message: 'should match some schema in anyOf'
                    };
                    if (vErrors === null) vErrors = [err];
                    else vErrors.push(err);
                    errors++;
                    MPGrant.errors = vErrors;
                    return false;
                  } else {
                    errors = errs__2;
                    if (vErrors !== null) {
                      if (errs__2) vErrors.length = errs__2;
                      else vErrors = null;
                    }
                  }
                  var valid2 = errors === errs_2;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data._id;
                if (data1 === undefined) {
                  valid1 = false;
                  MPGrant.errors = [{
                    keyword: 'required',
                    dataPath: (dataPath || '') + "",
                    schemaPath: '#/required',
                    params: {
                      missingProperty: '_id'
                    },
                    message: 'should have required property \'_id\''
                  }];
                  return false;
                } else {
                  var errs_1 = errors;
                  var errs_2 = errors;
                  if (typeof data1 === "string") {
                    if (!pattern0.test(data1)) {
                      MPGrant.errors = [{
                        keyword: 'pattern',
                        dataPath: (dataPath || '') + '._id',
                        schemaPath: 'strings.json#/definitions/_id/pattern',
                        params: {
                          pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                        },
                        message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                      }];
                      return false;
                    }
                  } else {
                    MPGrant.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: 'strings.json#/definitions/_id/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    }];
                    return false;
                  }
                  var valid2 = errors === errs_2;
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  if (data._rev === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    if (typeof data._rev !== "string") {
                      MPGrant.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '._rev',
                        schemaPath: '#/properties/_rev/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data._revisions;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                        var errs__1 = errors;
                        var valid2 = true;
                        var data2 = data1.start;
                        if (data2 === undefined) {
                          valid2 = true;
                        } else {
                          var errs_2 = errors;
                          if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                            MPGrant.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '._revisions.start',
                              schemaPath: '#/properties/_revisions/properties/start/type',
                              params: {
                                type: 'integer'
                              },
                              message: 'should be integer'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                        }
                        if (valid2) {
                          var data2 = data1.ids;
                          if (data2 === undefined) {
                            valid2 = true;
                          } else {
                            var errs_2 = errors;
                            if (Array.isArray(data2)) {
                              var errs__2 = errors;
                              var valid2;
                              for (var i2 = 0; i2 < data2.length; i2++) {
                                var errs_3 = errors;
                                if (typeof data2[i2] !== "string") {
                                  MPGrant.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                    schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid3 = errors === errs_3;
                                if (!valid3) break;
                              }
                            } else {
                              MPGrant.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids',
                                schemaPath: '#/properties/_revisions/properties/ids/type',
                                params: {
                                  type: 'array'
                                },
                                message: 'should be array'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                          }
                        }
                      } else {
                        MPGrant.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions',
                          schemaPath: '#/properties/_revisions/type',
                          params: {
                            type: 'object'
                          },
                          message: 'should be object'
                        }];
                        return false;
                      }
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      if (data.sessionID === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        if (typeof data.sessionID !== "string") {
                          MPGrant.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.sessionID',
                            schemaPath: '#/properties/sessionID/type',
                            params: {
                              type: 'string'
                            },
                            message: 'should be string'
                          }];
                          return false;
                        }
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data.createdAt;
                        if (data1 === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          var errs_2 = errors;
                          if (typeof data1 === "number") {
                            if (data1 > 2000000000 || data1 !== data1) {
                              MPGrant.errors = [{
                                keyword: 'maximum',
                                dataPath: (dataPath || '') + '.createdAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                params: {
                                  comparison: '<=',
                                  limit: 2000000000,
                                  exclusive: false
                                },
                                message: 'should be <= 2000000000'
                              }];
                              return false;
                            } else {
                              if (data1 < 0 || data1 !== data1) {
                                MPGrant.errors = [{
                                  keyword: 'minimum',
                                  dataPath: (dataPath || '') + '.createdAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                  params: {
                                    comparison: '>=',
                                    limit: 0,
                                    exclusive: false
                                  },
                                  message: 'should be >= 0'
                                }];
                                return false;
                              }
                            }
                          } else {
                            MPGrant.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.createdAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/type',
                              params: {
                                type: 'number'
                              },
                              message: 'should be number'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          var data1 = data.updatedAt;
                          if (data1 === undefined) {
                            valid1 = true;
                          } else {
                            var errs_1 = errors;
                            var errs_2 = errors;
                            if (typeof data1 === "number") {
                              if (data1 > 2000000000 || data1 !== data1) {
                                MPGrant.errors = [{
                                  keyword: 'maximum',
                                  dataPath: (dataPath || '') + '.updatedAt',
                                  schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                  params: {
                                    comparison: '<=',
                                    limit: 2000000000,
                                    exclusive: false
                                  },
                                  message: 'should be <= 2000000000'
                                }];
                                return false;
                              } else {
                                if (data1 < 0 || data1 !== data1) {
                                  MPGrant.errors = [{
                                    keyword: 'minimum',
                                    dataPath: (dataPath || '') + '.updatedAt',
                                    schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                    params: {
                                      comparison: '>=',
                                      limit: 0,
                                      exclusive: false
                                    },
                                    message: 'should be >= 0'
                                  }];
                                  return false;
                                }
                              }
                            } else {
                              MPGrant.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.updatedAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/type',
                                params: {
                                  type: 'number'
                                },
                                message: 'should be number'
                              }];
                              return false;
                            }
                            var valid2 = errors === errs_2;
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            var data1 = data.keywordIDs;
                            if (data1 === undefined) {
                              valid1 = true;
                            } else {
                              var errs_1 = errors;
                              if (Array.isArray(data1)) {
                                var errs__1 = errors;
                                var valid1;
                                for (var i1 = 0; i1 < data1.length; i1++) {
                                  var data2 = data1[i1];
                                  var errs_2 = errors;
                                  var errs_3 = errors;
                                  if (typeof data2 === "string") {
                                    if (!pattern4.test(data2)) {
                                      MPGrant.errors = [{
                                        keyword: 'pattern',
                                        dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                        schemaPath: 'strings.json#/definitions/keywordId/pattern',
                                        params: {
                                          pattern: '^MPKeyword:[0-9a-zA-Z\\-]+'
                                        },
                                        message: 'should match pattern "^MPKeyword:[0-9a-zA-Z\\-]+"'
                                      }];
                                      return false;
                                    }
                                  } else {
                                    MPGrant.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                      schemaPath: 'strings.json#/definitions/keywordId/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid3 = errors === errs_3;
                                  var valid2 = errors === errs_2;
                                  if (!valid2) break;
                                }
                              } else {
                                MPGrant.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.keywordIDs',
                                  schemaPath: '#/properties/keywordIDs/type',
                                  params: {
                                    type: 'array'
                                  },
                                  message: 'should be array'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              if (data.organization === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if (typeof data.organization !== "string") {
                                  MPGrant.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.organization',
                                    schemaPath: '#/properties/organization/type',
                                    params: {
                                      type: 'string'
                                    },
                                    message: 'should be string'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.code === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.code !== "string") {
                                    MPGrant.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.code',
                                      schemaPath: '#/properties/code/type',
                                      params: {
                                        type: 'string'
                                      },
                                      message: 'should be string'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                                if (valid1) {
                                  if (data.title === undefined) {
                                    valid1 = true;
                                  } else {
                                    var errs_1 = errors;
                                    if (typeof data.title !== "string") {
                                      MPGrant.errors = [{
                                        keyword: 'type',
                                        dataPath: (dataPath || '') + '.title',
                                        schemaPath: '#/properties/title/type',
                                        params: {
                                          type: 'string'
                                        },
                                        message: 'should be string'
                                      }];
                                      return false;
                                    }
                                    var valid1 = errors === errs_1;
                                  }
                                  if (valid1) {
                                    if (data.fundingBody === undefined) {
                                      valid1 = true;
                                    } else {
                                      var errs_1 = errors;
                                      if (!refVal4(data.fundingBody, (dataPath || '') + '.fundingBody', data, 'fundingBody', rootData)) {
                                        if (vErrors === null) vErrors = refVal4.errors;
                                        else vErrors = vErrors.concat(refVal4.errors);
                                        errors = vErrors.length;
                                      }
                                      var valid1 = errors === errs_1;
                                    }
                                    if (valid1) {
                                      var data1 = data.objectType;
                                      if (data1 === undefined) {
                                        valid1 = false;
                                        MPGrant.errors = [{
                                          keyword: 'required',
                                          dataPath: (dataPath || '') + "",
                                          schemaPath: '#/required',
                                          params: {
                                            missingProperty: 'objectType'
                                          },
                                          message: 'should have required property \'objectType\''
                                        }];
                                        return false;
                                      } else {
                                        var errs_1 = errors;
                                        if (typeof data1 !== "string") {
                                          MPGrant.errors = [{
                                            keyword: 'type',
                                            dataPath: (dataPath || '') + '.objectType',
                                            schemaPath: '#/properties/objectType/type',
                                            params: {
                                              type: 'string'
                                            },
                                            message: 'should be string'
                                          }];
                                          return false;
                                        }
                                        var schema1 = MPGrant.schema.properties.objectType.enum;
                                        var valid1;
                                        valid1 = false;
                                        for (var i1 = 0; i1 < schema1.length; i1++)
                                          if (equal(data1, schema1[i1])) {
                                            valid1 = true;
                                            break;
                                          }
                                        if (!valid1) {
                                          MPGrant.errors = [{
                                            keyword: 'enum',
                                            dataPath: (dataPath || '') + '.objectType',
                                            schemaPath: '#/properties/objectType/enum',
                                            params: {
                                              allowedValues: schema1
                                            },
                                            message: 'should be equal to one of the allowed values'
                                          }];
                                          return false;
                                        }
                                        var valid1 = errors === errs_1;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPGrant.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPGrant.errors = vErrors;
      return errors === 0;
    };
    MPGrant.schema = {
      "$id": "MPGrant.json",
      "additionalProperties": false,
      "properties": {
        "manuscript": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?Manuscript"
          }]
        },
        "container_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "anyOf": [{
              "type": "string",
              "pattern": "^(MP)?Project"
            }, {
              "type": "string",
              "pattern": "^(MP)?Library"
            }]
          }]
        },
        "_id": {
          "$ref": "strings.json#/definitions/_id"
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "sessionID": {
          "type": "string"
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "keywordIDs": {
          "type": "array",
          "items": {
            "$ref": "strings.json#/definitions/keywordId"
          }
        },
        "organization": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "fundingBody": {
          "$ref": "MPFundingBody.json#"
        },
        "objectType": {
          "type": "string",
          "enum": ["Grant", "MPGrant"]
        }
      },
      "required": ["manuscript", "container_id", "_id", "objectType"],
      "title": "Grant",
      "type": "object"
    };
    MPGrant.errors = null;

    function MPKeyword(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?Library');
      var pattern2 = new RegExp('^MPKeyword:[0-9a-zA-Z\\-]+');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "string",
        "pattern": "^MPKeyword:[0-9a-zA-Z\\-]+"
      };
      var refVal3 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      }; /*# sourceURL=MPKeyword.json */
      var vErrors = null;
      var errors = 0;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || key0 == 'library_id' || key0 == '_id' || key0 == '_rev' || key0 == '_revisions' || key0 == 'createdAt' || key0 == 'updatedAt' || key0 == 'objectType' || key0 == 'name');
            if (isAdditional0) {
              valid1 = false;
              MPKeyword.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data.library_id;
            if (data1 === undefined) {
              valid1 = false;
              MPKeyword.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: 'library_id'
                },
                message: 'should have required property \'library_id\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPKeyword.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '.library_id',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPKeyword.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '.library_id',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPKeyword.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '.library_id',
                      schemaPath: '#/properties/library_id/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?Library'
                      },
                      message: 'should match pattern "^(MP)?Library"'
                    }];
                    return false;
                  }
                } else {
                  MPKeyword.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '.library_id',
                    schemaPath: '#/properties/library_id/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              var data1 = data._id;
              if (data1 === undefined) {
                valid1 = false;
                MPKeyword.errors = [{
                  keyword: 'required',
                  dataPath: (dataPath || '') + "",
                  schemaPath: '#/required',
                  params: {
                    missingProperty: '_id'
                  },
                  message: 'should have required property \'_id\''
                }];
                return false;
              } else {
                var errs_1 = errors;
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern2.test(data1)) {
                    MPKeyword.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: 'strings.json#/definitions/keywordId/pattern',
                      params: {
                        pattern: '^MPKeyword:[0-9a-zA-Z\\-]+'
                      },
                      message: 'should match pattern "^MPKeyword:[0-9a-zA-Z\\-]+"'
                    }];
                    return false;
                  }
                } else {
                  MPKeyword.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: 'strings.json#/definitions/keywordId/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                if (data._rev === undefined) {
                  valid1 = true;
                } else {
                  var errs_1 = errors;
                  if (typeof data._rev !== "string") {
                    MPKeyword.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._rev',
                      schemaPath: '#/properties/_rev/type',
                      params: {
                        type: 'string'
                      },
                      message: 'should be string'
                    }];
                    return false;
                  }
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  var data1 = data._revisions;
                  if (data1 === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                      var errs__1 = errors;
                      var valid2 = true;
                      var data2 = data1.start;
                      if (data2 === undefined) {
                        valid2 = true;
                      } else {
                        var errs_2 = errors;
                        if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                          MPKeyword.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '._revisions.start',
                            schemaPath: '#/properties/_revisions/properties/start/type',
                            params: {
                              type: 'integer'
                            },
                            message: 'should be integer'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                      }
                      if (valid2) {
                        var data2 = data1.ids;
                        if (data2 === undefined) {
                          valid2 = true;
                        } else {
                          var errs_2 = errors;
                          if (Array.isArray(data2)) {
                            var errs__2 = errors;
                            var valid2;
                            for (var i2 = 0; i2 < data2.length; i2++) {
                              var errs_3 = errors;
                              if (typeof data2[i2] !== "string") {
                                MPKeyword.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                  schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                  params: {
                                    type: 'string'
                                  },
                                  message: 'should be string'
                                }];
                                return false;
                              }
                              var valid3 = errors === errs_3;
                              if (!valid3) break;
                            }
                          } else {
                            MPKeyword.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '._revisions.ids',
                              schemaPath: '#/properties/_revisions/properties/ids/type',
                              params: {
                                type: 'array'
                              },
                              message: 'should be array'
                            }];
                            return false;
                          }
                          var valid2 = errors === errs_2;
                        }
                      }
                    } else {
                      MPKeyword.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '._revisions',
                        schemaPath: '#/properties/_revisions/type',
                        params: {
                          type: 'object'
                        },
                        message: 'should be object'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data.createdAt;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      var errs_2 = errors;
                      if (typeof data1 === "number") {
                        if (data1 > 2000000000 || data1 !== data1) {
                          MPKeyword.errors = [{
                            keyword: 'maximum',
                            dataPath: (dataPath || '') + '.createdAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                            params: {
                              comparison: '<=',
                              limit: 2000000000,
                              exclusive: false
                            },
                            message: 'should be <= 2000000000'
                          }];
                          return false;
                        } else {
                          if (data1 < 0 || data1 !== data1) {
                            MPKeyword.errors = [{
                              keyword: 'minimum',
                              dataPath: (dataPath || '') + '.createdAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                              params: {
                                comparison: '>=',
                                limit: 0,
                                exclusive: false
                              },
                              message: 'should be >= 0'
                            }];
                            return false;
                          }
                        }
                      } else {
                        MPKeyword.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '.createdAt',
                          schemaPath: 'numbers.json#/definitions/timestamp/type',
                          params: {
                            type: 'number'
                          },
                          message: 'should be number'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      var data1 = data.updatedAt;
                      if (data1 === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        var errs_2 = errors;
                        if (typeof data1 === "number") {
                          if (data1 > 2000000000 || data1 !== data1) {
                            MPKeyword.errors = [{
                              keyword: 'maximum',
                              dataPath: (dataPath || '') + '.updatedAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                              params: {
                                comparison: '<=',
                                limit: 2000000000,
                                exclusive: false
                              },
                              message: 'should be <= 2000000000'
                            }];
                            return false;
                          } else {
                            if (data1 < 0 || data1 !== data1) {
                              MPKeyword.errors = [{
                                keyword: 'minimum',
                                dataPath: (dataPath || '') + '.updatedAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                params: {
                                  comparison: '>=',
                                  limit: 0,
                                  exclusive: false
                                },
                                message: 'should be >= 0'
                              }];
                              return false;
                            }
                          }
                        } else {
                          MPKeyword.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.updatedAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/type',
                            params: {
                              type: 'number'
                            },
                            message: 'should be number'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data.objectType;
                        if (data1 === undefined) {
                          valid1 = false;
                          MPKeyword.errors = [{
                            keyword: 'required',
                            dataPath: (dataPath || '') + "",
                            schemaPath: '#/required',
                            params: {
                              missingProperty: 'objectType'
                            },
                            message: 'should have required property \'objectType\''
                          }];
                          return false;
                        } else {
                          var errs_1 = errors;
                          if (typeof data1 !== "string") {
                            MPKeyword.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.objectType',
                              schemaPath: '#/properties/objectType/type',
                              params: {
                                type: 'string'
                              },
                              message: 'should be string'
                            }];
                            return false;
                          }
                          var schema1 = MPKeyword.schema.properties.objectType.enum;
                          var valid1;
                          valid1 = false;
                          for (var i1 = 0; i1 < schema1.length; i1++)
                            if (equal(data1, schema1[i1])) {
                              valid1 = true;
                              break;
                            }
                          if (!valid1) {
                            MPKeyword.errors = [{
                              keyword: 'enum',
                              dataPath: (dataPath || '') + '.objectType',
                              schemaPath: '#/properties/objectType/enum',
                              params: {
                                allowedValues: schema1
                              },
                              message: 'should be equal to one of the allowed values'
                            }];
                            return false;
                          }
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          if (data.name === undefined) {
                            valid1 = false;
                            MPKeyword.errors = [{
                              keyword: 'required',
                              dataPath: (dataPath || '') + "",
                              schemaPath: '#/required',
                              params: {
                                missingProperty: 'name'
                              },
                              message: 'should have required property \'name\''
                            }];
                            return false;
                          } else {
                            var errs_1 = errors;
                            if (typeof data.name !== "string") {
                              MPKeyword.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.name',
                                schemaPath: '#/properties/name/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var valid1 = errors === errs_1;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPKeyword.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPKeyword.errors = vErrors;
      return errors === 0;
    };
    MPKeyword.schema = {
      "$id": "MPKeyword.json",
      "additionalProperties": false,
      "properties": {
        "library_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?Library"
          }]
        },
        "_id": {
          "$ref": "strings.json#/definitions/keywordId"
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "objectType": {
          "type": "string",
          "enum": ["Keyword", "MPKeyword"]
        },
        "name": {
          "type": "string"
        }
      },
      "required": ["_id", "library_id", "objectType", "name"],
      "type": "object"
    };
    MPKeyword.errors = null;

    function MPLibrary(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^(MP)?Library');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      }; /*# sourceURL=MPLibrary.json */
      var vErrors = null;
      var errors = 0;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || key0 == '_id' || key0 == '_rev' || key0 == '_revisions' || key0 == 'objectType');
            if (isAdditional0) {
              valid1 = false;
              MPLibrary.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data._id;
            if (data1 === undefined) {
              valid1 = false;
              MPLibrary.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: '_id'
                },
                message: 'should have required property \'_id\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              var errs_3 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPLibrary.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPLibrary.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '._id',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid3 = errors === errs_3;
              var valid2 = errors === errs_2;
              if (valid2) {
                var errs_2 = errors;
                if (typeof data1 === "string") {
                  if (!pattern1.test(data1)) {
                    MPLibrary.errors = [{
                      keyword: 'pattern',
                      dataPath: (dataPath || '') + '._id',
                      schemaPath: '#/properties/_id/allOf/1/pattern',
                      params: {
                        pattern: '^(MP)?Library'
                      },
                      message: 'should match pattern "^(MP)?Library"'
                    }];
                    return false;
                  }
                } else {
                  MPLibrary.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: '#/properties/_id/allOf/1/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid2 = errors === errs_2;
              }
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              if (data._rev === undefined) {
                valid1 = true;
              } else {
                var errs_1 = errors;
                if (typeof data._rev !== "string") {
                  MPLibrary.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._rev',
                    schemaPath: '#/properties/_rev/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data._revisions;
                if (data1 === undefined) {
                  valid1 = true;
                } else {
                  var errs_1 = errors;
                  if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                    var errs__1 = errors;
                    var valid2 = true;
                    var data2 = data1.start;
                    if (data2 === undefined) {
                      valid2 = true;
                    } else {
                      var errs_2 = errors;
                      if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                        MPLibrary.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions.start',
                          schemaPath: '#/properties/_revisions/properties/start/type',
                          params: {
                            type: 'integer'
                          },
                          message: 'should be integer'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                    }
                    if (valid2) {
                      var data2 = data1.ids;
                      if (data2 === undefined) {
                        valid2 = true;
                      } else {
                        var errs_2 = errors;
                        if (Array.isArray(data2)) {
                          var errs__2 = errors;
                          var valid2;
                          for (var i2 = 0; i2 < data2.length; i2++) {
                            var errs_3 = errors;
                            if (typeof data2[i2] !== "string") {
                              MPLibrary.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var valid3 = errors === errs_3;
                            if (!valid3) break;
                          }
                        } else {
                          MPLibrary.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '._revisions.ids',
                            schemaPath: '#/properties/_revisions/properties/ids/type',
                            params: {
                              type: 'array'
                            },
                            message: 'should be array'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                      }
                    }
                  } else {
                    MPLibrary.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._revisions',
                      schemaPath: '#/properties/_revisions/type',
                      params: {
                        type: 'object'
                      },
                      message: 'should be object'
                    }];
                    return false;
                  }
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  var data1 = data.objectType;
                  if (data1 === undefined) {
                    valid1 = false;
                    MPLibrary.errors = [{
                      keyword: 'required',
                      dataPath: (dataPath || '') + "",
                      schemaPath: '#/required',
                      params: {
                        missingProperty: 'objectType'
                      },
                      message: 'should have required property \'objectType\''
                    }];
                    return false;
                  } else {
                    var errs_1 = errors;
                    if (typeof data1 !== "string") {
                      MPLibrary.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.objectType',
                        schemaPath: '#/properties/objectType/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      }];
                      return false;
                    }
                    var schema1 = MPLibrary.schema.properties.objectType.enum;
                    var valid1;
                    valid1 = false;
                    for (var i1 = 0; i1 < schema1.length; i1++)
                      if (equal(data1, schema1[i1])) {
                        valid1 = true;
                        break;
                      }
                    if (!valid1) {
                      MPLibrary.errors = [{
                        keyword: 'enum',
                        dataPath: (dataPath || '') + '.objectType',
                        schemaPath: '#/properties/objectType/enum',
                        params: {
                          allowedValues: schema1
                        },
                        message: 'should be equal to one of the allowed values'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                }
              }
            }
          }
        }
      } else {
        MPLibrary.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPLibrary.errors = vErrors;
      return errors === 0;
    };
    MPLibrary.schema = {
      "$id": "MPLibrary.json",
      "additionalProperties": false,
      "properties": {
        "_id": {
          "allOf": [{
            "$ref": "strings.json#/definitions/_id"
          }, {
            "type": "string",
            "pattern": "^(MP)?Library"
          }]
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "objectType": {
          "type": "string",
          "enum": ["Library", "MPLibrary"]
        }
      },
      "required": ["_id", "objectType"],
      "type": "object"
    };
    MPLibrary.errors = null;

    function MPUserProfile(data, dataPath, parentData, parentDataProperty, rootData) {
      var pattern0 = new RegExp('^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+');
      var pattern1 = new RegExp('^MPKeyword:[0-9a-zA-Z\\-]+');
      var refVal1 = {
        "type": "string",
        "pattern": "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"
      };
      var refVal2 = {
        "type": "number",
        "minimum": 0,
        "maximum": 2000000000
      };
      var refVal3 = {
        "type": "string",
        "pattern": "^MPKeyword:[0-9a-zA-Z\\-]+"
      };
      var refVal4 = MPBibliographicName;
      var refVal5 = MPAffiliation;
      var refVal6 = MPGrant; /*# sourceURL=MPUserProfile.json */
      var vErrors = null;
      var errors = 0;
      if (rootData === undefined) rootData = data;
      if ((data && typeof data === "object" && !Array.isArray(data))) {
        if (true) {
          var errs__0 = errors;
          var valid1 = true;
          for (var key0 in data) {
            var isAdditional0 = !(false || MPUserProfile.schema.properties.hasOwnProperty(key0));
            if (isAdditional0) {
              valid1 = false;
              MPUserProfile.errors = [{
                keyword: 'additionalProperties',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/additionalProperties',
                params: {
                  additionalProperty: '' + key0 + ''
                },
                message: 'should NOT have additional properties'
              }];
              return false;
              break;
            }
          }
          if (valid1) {
            var data1 = data._id;
            if (data1 === undefined) {
              valid1 = false;
              MPUserProfile.errors = [{
                keyword: 'required',
                dataPath: (dataPath || '') + "",
                schemaPath: '#/required',
                params: {
                  missingProperty: '_id'
                },
                message: 'should have required property \'_id\''
              }];
              return false;
            } else {
              var errs_1 = errors;
              var errs_2 = errors;
              if (typeof data1 === "string") {
                if (!pattern0.test(data1)) {
                  MPUserProfile.errors = [{
                    keyword: 'pattern',
                    dataPath: (dataPath || '') + '._id',
                    schemaPath: 'strings.json#/definitions/_id/pattern',
                    params: {
                      pattern: '^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+'
                    },
                    message: 'should match pattern "^[A-Z][a-zA-Z]+:[0-9a-zA-Z\\-]+"'
                  }];
                  return false;
                }
              } else {
                MPUserProfile.errors = [{
                  keyword: 'type',
                  dataPath: (dataPath || '') + '._id',
                  schemaPath: 'strings.json#/definitions/_id/type',
                  params: {
                    type: 'string'
                  },
                  message: 'should be string'
                }];
                return false;
              }
              var valid2 = errors === errs_2;
              var valid1 = errors === errs_1;
            }
            if (valid1) {
              if (data._rev === undefined) {
                valid1 = true;
              } else {
                var errs_1 = errors;
                if (typeof data._rev !== "string") {
                  MPUserProfile.errors = [{
                    keyword: 'type',
                    dataPath: (dataPath || '') + '._rev',
                    schemaPath: '#/properties/_rev/type',
                    params: {
                      type: 'string'
                    },
                    message: 'should be string'
                  }];
                  return false;
                }
                var valid1 = errors === errs_1;
              }
              if (valid1) {
                var data1 = data._revisions;
                if (data1 === undefined) {
                  valid1 = true;
                } else {
                  var errs_1 = errors;
                  if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                    var errs__1 = errors;
                    var valid2 = true;
                    var data2 = data1.start;
                    if (data2 === undefined) {
                      valid2 = true;
                    } else {
                      var errs_2 = errors;
                      if ((typeof data2 !== "number" || (data2 % 1) || data2 !== data2)) {
                        MPUserProfile.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '._revisions.start',
                          schemaPath: '#/properties/_revisions/properties/start/type',
                          params: {
                            type: 'integer'
                          },
                          message: 'should be integer'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                    }
                    if (valid2) {
                      var data2 = data1.ids;
                      if (data2 === undefined) {
                        valid2 = true;
                      } else {
                        var errs_2 = errors;
                        if (Array.isArray(data2)) {
                          var errs__2 = errors;
                          var valid2;
                          for (var i2 = 0; i2 < data2.length; i2++) {
                            var errs_3 = errors;
                            if (typeof data2[i2] !== "string") {
                              MPUserProfile.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '._revisions.ids[' + i2 + ']',
                                schemaPath: '#/properties/_revisions/properties/ids/items/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var valid3 = errors === errs_3;
                            if (!valid3) break;
                          }
                        } else {
                          MPUserProfile.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '._revisions.ids',
                            schemaPath: '#/properties/_revisions/properties/ids/type',
                            params: {
                              type: 'array'
                            },
                            message: 'should be array'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                      }
                    }
                  } else {
                    MPUserProfile.errors = [{
                      keyword: 'type',
                      dataPath: (dataPath || '') + '._revisions',
                      schemaPath: '#/properties/_revisions/type',
                      params: {
                        type: 'object'
                      },
                      message: 'should be object'
                    }];
                    return false;
                  }
                  var valid1 = errors === errs_1;
                }
                if (valid1) {
                  if (data.sessionID === undefined) {
                    valid1 = true;
                  } else {
                    var errs_1 = errors;
                    if (typeof data.sessionID !== "string") {
                      MPUserProfile.errors = [{
                        keyword: 'type',
                        dataPath: (dataPath || '') + '.sessionID',
                        schemaPath: '#/properties/sessionID/type',
                        params: {
                          type: 'string'
                        },
                        message: 'should be string'
                      }];
                      return false;
                    }
                    var valid1 = errors === errs_1;
                  }
                  if (valid1) {
                    var data1 = data.createdAt;
                    if (data1 === undefined) {
                      valid1 = true;
                    } else {
                      var errs_1 = errors;
                      var errs_2 = errors;
                      if (typeof data1 === "number") {
                        if (data1 > 2000000000 || data1 !== data1) {
                          MPUserProfile.errors = [{
                            keyword: 'maximum',
                            dataPath: (dataPath || '') + '.createdAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                            params: {
                              comparison: '<=',
                              limit: 2000000000,
                              exclusive: false
                            },
                            message: 'should be <= 2000000000'
                          }];
                          return false;
                        } else {
                          if (data1 < 0 || data1 !== data1) {
                            MPUserProfile.errors = [{
                              keyword: 'minimum',
                              dataPath: (dataPath || '') + '.createdAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                              params: {
                                comparison: '>=',
                                limit: 0,
                                exclusive: false
                              },
                              message: 'should be >= 0'
                            }];
                            return false;
                          }
                        }
                      } else {
                        MPUserProfile.errors = [{
                          keyword: 'type',
                          dataPath: (dataPath || '') + '.createdAt',
                          schemaPath: 'numbers.json#/definitions/timestamp/type',
                          params: {
                            type: 'number'
                          },
                          message: 'should be number'
                        }];
                        return false;
                      }
                      var valid2 = errors === errs_2;
                      var valid1 = errors === errs_1;
                    }
                    if (valid1) {
                      var data1 = data.updatedAt;
                      if (data1 === undefined) {
                        valid1 = true;
                      } else {
                        var errs_1 = errors;
                        var errs_2 = errors;
                        if (typeof data1 === "number") {
                          if (data1 > 2000000000 || data1 !== data1) {
                            MPUserProfile.errors = [{
                              keyword: 'maximum',
                              dataPath: (dataPath || '') + '.updatedAt',
                              schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                              params: {
                                comparison: '<=',
                                limit: 2000000000,
                                exclusive: false
                              },
                              message: 'should be <= 2000000000'
                            }];
                            return false;
                          } else {
                            if (data1 < 0 || data1 !== data1) {
                              MPUserProfile.errors = [{
                                keyword: 'minimum',
                                dataPath: (dataPath || '') + '.updatedAt',
                                schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                params: {
                                  comparison: '>=',
                                  limit: 0,
                                  exclusive: false
                                },
                                message: 'should be >= 0'
                              }];
                              return false;
                            }
                          }
                        } else {
                          MPUserProfile.errors = [{
                            keyword: 'type',
                            dataPath: (dataPath || '') + '.updatedAt',
                            schemaPath: 'numbers.json#/definitions/timestamp/type',
                            params: {
                              type: 'number'
                            },
                            message: 'should be number'
                          }];
                          return false;
                        }
                        var valid2 = errors === errs_2;
                        var valid1 = errors === errs_1;
                      }
                      if (valid1) {
                        var data1 = data.keywordIDs;
                        if (data1 === undefined) {
                          valid1 = true;
                        } else {
                          var errs_1 = errors;
                          if (Array.isArray(data1)) {
                            var errs__1 = errors;
                            var valid1;
                            for (var i1 = 0; i1 < data1.length; i1++) {
                              var data2 = data1[i1];
                              var errs_2 = errors;
                              var errs_3 = errors;
                              if (typeof data2 === "string") {
                                if (!pattern1.test(data2)) {
                                  MPUserProfile.errors = [{
                                    keyword: 'pattern',
                                    dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                    schemaPath: 'strings.json#/definitions/keywordId/pattern',
                                    params: {
                                      pattern: '^MPKeyword:[0-9a-zA-Z\\-]+'
                                    },
                                    message: 'should match pattern "^MPKeyword:[0-9a-zA-Z\\-]+"'
                                  }];
                                  return false;
                                }
                              } else {
                                MPUserProfile.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.keywordIDs[' + i1 + ']',
                                  schemaPath: 'strings.json#/definitions/keywordId/type',
                                  params: {
                                    type: 'string'
                                  },
                                  message: 'should be string'
                                }];
                                return false;
                              }
                              var valid3 = errors === errs_3;
                              var valid2 = errors === errs_2;
                              if (!valid2) break;
                            }
                          } else {
                            MPUserProfile.errors = [{
                              keyword: 'type',
                              dataPath: (dataPath || '') + '.keywordIDs',
                              schemaPath: '#/properties/keywordIDs/type',
                              params: {
                                type: 'array'
                              },
                              message: 'should be array'
                            }];
                            return false;
                          }
                          var valid1 = errors === errs_1;
                        }
                        if (valid1) {
                          if (data.role === undefined) {
                            valid1 = true;
                          } else {
                            var errs_1 = errors;
                            if (typeof data.role !== "string") {
                              MPUserProfile.errors = [{
                                keyword: 'type',
                                dataPath: (dataPath || '') + '.role',
                                schemaPath: '#/properties/role/type',
                                params: {
                                  type: 'string'
                                },
                                message: 'should be string'
                              }];
                              return false;
                            }
                            var valid1 = errors === errs_1;
                          }
                          if (valid1) {
                            if (data.isMe === undefined) {
                              valid1 = true;
                            } else {
                              var errs_1 = errors;
                              if (typeof data.isMe !== "boolean") {
                                MPUserProfile.errors = [{
                                  keyword: 'type',
                                  dataPath: (dataPath || '') + '.isMe',
                                  schemaPath: '#/properties/isMe/type',
                                  params: {
                                    type: 'boolean'
                                  },
                                  message: 'should be boolean'
                                }];
                                return false;
                              }
                              var valid1 = errors === errs_1;
                            }
                            if (valid1) {
                              var data1 = data.category;
                              if (data1 === undefined) {
                                valid1 = true;
                              } else {
                                var errs_1 = errors;
                                if ((data1 && typeof data1 === "object" && !Array.isArray(data1))) {
                                  var errs__1 = errors;
                                  var valid2 = true;
                                  for (var key1 in data1) {
                                    var isAdditional1 = !(false || key1 == 'name' || key1 == 'desc' || key1 == 'priority');
                                    if (isAdditional1) {
                                      valid2 = false;
                                      MPUserProfile.errors = [{
                                        keyword: 'additionalProperties',
                                        dataPath: (dataPath || '') + '.category',
                                        schemaPath: '#/properties/category/additionalProperties',
                                        params: {
                                          additionalProperty: '' + key1 + ''
                                        },
                                        message: 'should NOT have additional properties'
                                      }];
                                      return false;
                                      break;
                                    }
                                  }
                                  if (valid2) {
                                    if (data1.name === undefined) {
                                      valid2 = true;
                                    } else {
                                      var errs_2 = errors;
                                      if (typeof data1.name !== "string") {
                                        MPUserProfile.errors = [{
                                          keyword: 'type',
                                          dataPath: (dataPath || '') + '.category.name',
                                          schemaPath: '#/properties/category/properties/name/type',
                                          params: {
                                            type: 'string'
                                          },
                                          message: 'should be string'
                                        }];
                                        return false;
                                      }
                                      var valid2 = errors === errs_2;
                                    }
                                    if (valid2) {
                                      if (data1.desc === undefined) {
                                        valid2 = true;
                                      } else {
                                        var errs_2 = errors;
                                        if (typeof data1.desc !== "string") {
                                          MPUserProfile.errors = [{
                                            keyword: 'type',
                                            dataPath: (dataPath || '') + '.category.desc',
                                            schemaPath: '#/properties/category/properties/desc/type',
                                            params: {
                                              type: 'string'
                                            },
                                            message: 'should be string'
                                          }];
                                          return false;
                                        }
                                        var valid2 = errors === errs_2;
                                      }
                                      if (valid2) {
                                        if (data1.priority === undefined) {
                                          valid2 = true;
                                        } else {
                                          var errs_2 = errors;
                                          if (typeof data1.priority !== "number") {
                                            MPUserProfile.errors = [{
                                              keyword: 'type',
                                              dataPath: (dataPath || '') + '.category.priority',
                                              schemaPath: '#/properties/category/properties/priority/type',
                                              params: {
                                                type: 'number'
                                              },
                                              message: 'should be number'
                                            }];
                                            return false;
                                          }
                                          var valid2 = errors === errs_2;
                                        }
                                      }
                                    }
                                  }
                                } else {
                                  MPUserProfile.errors = [{
                                    keyword: 'type',
                                    dataPath: (dataPath || '') + '.category',
                                    schemaPath: '#/properties/category/type',
                                    params: {
                                      type: 'object'
                                    },
                                    message: 'should be object'
                                  }];
                                  return false;
                                }
                                var valid1 = errors === errs_1;
                              }
                              if (valid1) {
                                if (data.priority === undefined) {
                                  valid1 = true;
                                } else {
                                  var errs_1 = errors;
                                  if (typeof data.priority !== "number") {
                                    MPUserProfile.errors = [{
                                      keyword: 'type',
                                      dataPath: (dataPath || '') + '.priority',
                                      schemaPath: '#/properties/priority/type',
                                      params: {
                                        type: 'number'
                                      },
                                      message: 'should be number'
                                    }];
                                    return false;
                                  }
                                  var valid1 = errors === errs_1;
                                }
                                if (valid1) {
                                  if (data.contribution === undefined) {
                                    valid1 = true;
                                  } else {
                                    var errs_1 = errors;
                                    if (typeof data.contribution !== "string") {
                                      MPUserProfile.errors = [{
                                        keyword: 'type',
                                        dataPath: (dataPath || '') + '.contribution',
                                        schemaPath: '#/properties/contribution/type',
                                        params: {
                                          type: 'string'
                                        },
                                        message: 'should be string'
                                      }];
                                      return false;
                                    }
                                    var valid1 = errors === errs_1;
                                  }
                                  if (valid1) {
                                    if (data.placeholderString === undefined) {
                                      valid1 = true;
                                    } else {
                                      var errs_1 = errors;
                                      if (typeof data.placeholderString !== "string") {
                                        MPUserProfile.errors = [{
                                          keyword: 'type',
                                          dataPath: (dataPath || '') + '.placeholderString',
                                          schemaPath: '#/properties/placeholderString/type',
                                          params: {
                                            type: 'string'
                                          },
                                          message: 'should be string'
                                        }];
                                        return false;
                                      }
                                      var valid1 = errors === errs_1;
                                    }
                                    if (valid1) {
                                      var data1 = data.appInvitationDate;
                                      if (data1 === undefined) {
                                        valid1 = true;
                                      } else {
                                        var errs_1 = errors;
                                        var errs_2 = errors;
                                        if (typeof data1 === "number") {
                                          if (data1 > 2000000000 || data1 !== data1) {
                                            MPUserProfile.errors = [{
                                              keyword: 'maximum',
                                              dataPath: (dataPath || '') + '.appInvitationDate',
                                              schemaPath: 'numbers.json#/definitions/timestamp/maximum',
                                              params: {
                                                comparison: '<=',
                                                limit: 2000000000,
                                                exclusive: false
                                              },
                                              message: 'should be <= 2000000000'
                                            }];
                                            return false;
                                          } else {
                                            if (data1 < 0 || data1 !== data1) {
                                              MPUserProfile.errors = [{
                                                keyword: 'minimum',
                                                dataPath: (dataPath || '') + '.appInvitationDate',
                                                schemaPath: 'numbers.json#/definitions/timestamp/minimum',
                                                params: {
                                                  comparison: '>=',
                                                  limit: 0,
                                                  exclusive: false
                                                },
                                                message: 'should be >= 0'
                                              }];
                                              return false;
                                            }
                                          }
                                        } else {
                                          MPUserProfile.errors = [{
                                            keyword: 'type',
                                            dataPath: (dataPath || '') + '.appInvitationDate',
                                            schemaPath: 'numbers.json#/definitions/timestamp/type',
                                            params: {
                                              type: 'number'
                                            },
                                            message: 'should be number'
                                          }];
                                          return false;
                                        }
                                        var valid2 = errors === errs_2;
                                        var valid1 = errors === errs_1;
                                      }
                                      if (valid1) {
                                        var data1 = data.addressBookIDs;
                                        if (data1 === undefined) {
                                          valid1 = true;
                                        } else {
                                          var errs_1 = errors;
                                          if (Array.isArray(data1)) {
                                            var errs__1 = errors;
                                            var valid1;
                                            for (var i1 = 0; i1 < data1.length; i1++) {
                                              var errs_2 = errors;
                                              if (typeof data1[i1] !== "string") {
                                                MPUserProfile.errors = [{
                                                  keyword: 'type',
                                                  dataPath: (dataPath || '') + '.addressBookIDs[' + i1 + ']',
                                                  schemaPath: '#/properties/addressBookIDs/items/type',
                                                  params: {
                                                    type: 'string'
                                                  },
                                                  message: 'should be string'
                                                }];
                                                return false;
                                              }
                                              var valid2 = errors === errs_2;
                                              if (!valid2) break;
                                            }
                                          } else {
                                            MPUserProfile.errors = [{
                                              keyword: 'type',
                                              dataPath: (dataPath || '') + '.addressBookIDs',
                                              schemaPath: '#/properties/addressBookIDs/type',
                                              params: {
                                                type: 'array'
                                              },
                                              message: 'should be array'
                                            }];
                                            return false;
                                          }
                                          var valid1 = errors === errs_1;
                                        }
                                        if (valid1) {
                                          var data1 = data.objectType;
                                          if (data1 === undefined) {
                                            valid1 = false;
                                            MPUserProfile.errors = [{
                                              keyword: 'required',
                                              dataPath: (dataPath || '') + "",
                                              schemaPath: '#/required',
                                              params: {
                                                missingProperty: 'objectType'
                                              },
                                              message: 'should have required property \'objectType\''
                                            }];
                                            return false;
                                          } else {
                                            var errs_1 = errors;
                                            if (typeof data1 !== "string") {
                                              MPUserProfile.errors = [{
                                                keyword: 'type',
                                                dataPath: (dataPath || '') + '.objectType',
                                                schemaPath: '#/properties/objectType/type',
                                                params: {
                                                  type: 'string'
                                                },
                                                message: 'should be string'
                                              }];
                                              return false;
                                            }
                                            var schema1 = MPUserProfile.schema.properties.objectType.enum;
                                            var valid1;
                                            valid1 = false;
                                            for (var i1 = 0; i1 < schema1.length; i1++)
                                              if (equal(data1, schema1[i1])) {
                                                valid1 = true;
                                                break;
                                              }
                                            if (!valid1) {
                                              MPUserProfile.errors = [{
                                                keyword: 'enum',
                                                dataPath: (dataPath || '') + '.objectType',
                                                schemaPath: '#/properties/objectType/enum',
                                                params: {
                                                  allowedValues: schema1
                                                },
                                                message: 'should be equal to one of the allowed values'
                                              }];
                                              return false;
                                            }
                                            var valid1 = errors === errs_1;
                                          }
                                          if (valid1) {
                                            if (data.bibliographicName === undefined) {
                                              valid1 = true;
                                            } else {
                                              var errs_1 = errors;
                                              if (!refVal4(data.bibliographicName, (dataPath || '') + '.bibliographicName', data, 'bibliographicName', rootData)) {
                                                if (vErrors === null) vErrors = refVal4.errors;
                                                else vErrors = vErrors.concat(refVal4.errors);
                                                errors = vErrors.length;
                                              }
                                              var valid1 = errors === errs_1;
                                            }
                                            if (valid1) {
                                              if (data.bio === undefined) {
                                                valid1 = true;
                                              } else {
                                                var errs_1 = errors;
                                                if (typeof data.bio !== "string") {
                                                  MPUserProfile.errors = [{
                                                    keyword: 'type',
                                                    dataPath: (dataPath || '') + '.bio',
                                                    schemaPath: '#/properties/bio/type',
                                                    params: {
                                                      type: 'string'
                                                    },
                                                    message: 'should be string'
                                                  }];
                                                  return false;
                                                }
                                                var valid1 = errors === errs_1;
                                              }
                                              if (valid1) {
                                                var data1 = data.affiliations;
                                                if (data1 === undefined) {
                                                  valid1 = true;
                                                } else {
                                                  var errs_1 = errors;
                                                  if (Array.isArray(data1)) {
                                                    var errs__1 = errors;
                                                    var valid1;
                                                    for (var i1 = 0; i1 < data1.length; i1++) {
                                                      var errs_2 = errors;
                                                      if (!refVal5(data1[i1], (dataPath || '') + '.affiliations[' + i1 + ']', data1, i1, rootData)) {
                                                        if (vErrors === null) vErrors = refVal5.errors;
                                                        else vErrors = vErrors.concat(refVal5.errors);
                                                        errors = vErrors.length;
                                                      }
                                                      var valid2 = errors === errs_2;
                                                      if (!valid2) break;
                                                    }
                                                  } else {
                                                    MPUserProfile.errors = [{
                                                      keyword: 'type',
                                                      dataPath: (dataPath || '') + '.affiliations',
                                                      schemaPath: '#/properties/affiliations/type',
                                                      params: {
                                                        type: 'array'
                                                      },
                                                      message: 'should be array'
                                                    }];
                                                    return false;
                                                  }
                                                  var valid1 = errors === errs_1;
                                                }
                                                if (valid1) {
                                                  var data1 = data.grants;
                                                  if (data1 === undefined) {
                                                    valid1 = true;
                                                  } else {
                                                    var errs_1 = errors;
                                                    if (Array.isArray(data1)) {
                                                      var errs__1 = errors;
                                                      var valid1;
                                                      for (var i1 = 0; i1 < data1.length; i1++) {
                                                        var errs_2 = errors;
                                                        if (!refVal6(data1[i1], (dataPath || '') + '.grants[' + i1 + ']', data1, i1, rootData)) {
                                                          if (vErrors === null) vErrors = refVal6.errors;
                                                          else vErrors = vErrors.concat(refVal6.errors);
                                                          errors = vErrors.length;
                                                        }
                                                        var valid2 = errors === errs_2;
                                                        if (!valid2) break;
                                                      }
                                                    } else {
                                                      MPUserProfile.errors = [{
                                                        keyword: 'type',
                                                        dataPath: (dataPath || '') + '.grants',
                                                        schemaPath: '#/properties/grants/type',
                                                        params: {
                                                          type: 'array'
                                                        },
                                                        message: 'should be array'
                                                      }];
                                                      return false;
                                                    }
                                                    var valid1 = errors === errs_1;
                                                  }
                                                  if (valid1) {
                                                    if (data.email === undefined) {
                                                      valid1 = true;
                                                    } else {
                                                      var errs_1 = errors;
                                                      if (typeof data.email !== "string") {
                                                        MPUserProfile.errors = [{
                                                          keyword: 'type',
                                                          dataPath: (dataPath || '') + '.email',
                                                          schemaPath: '#/properties/email/type',
                                                          params: {
                                                            type: 'string'
                                                          },
                                                          message: 'should be string'
                                                        }];
                                                        return false;
                                                      }
                                                      var valid1 = errors === errs_1;
                                                    }
                                                    if (valid1) {
                                                      if (data.jointContributorGroup === undefined) {
                                                        valid1 = true;
                                                      } else {
                                                        var errs_1 = errors;
                                                        if (typeof data.jointContributorGroup !== "number") {
                                                          MPUserProfile.errors = [{
                                                            keyword: 'type',
                                                            dataPath: (dataPath || '') + '.jointContributorGroup',
                                                            schemaPath: '#/properties/jointContributorGroup/type',
                                                            params: {
                                                              type: 'number'
                                                            },
                                                            message: 'should be number'
                                                          }];
                                                          return false;
                                                        }
                                                        var valid1 = errors === errs_1;
                                                      }
                                                      if (valid1) {
                                                        if (data.user_id === undefined) {
                                                          valid1 = false;
                                                          MPUserProfile.errors = [{
                                                            keyword: 'required',
                                                            dataPath: (dataPath || '') + "",
                                                            schemaPath: '#/required',
                                                            params: {
                                                              missingProperty: 'user_id'
                                                            },
                                                            message: 'should have required property \'user_id\''
                                                          }];
                                                          return false;
                                                        } else {
                                                          var errs_1 = errors;
                                                          if (typeof data.user_id !== "string") {
                                                            MPUserProfile.errors = [{
                                                              keyword: 'type',
                                                              dataPath: (dataPath || '') + '.user_id',
                                                              schemaPath: '#/properties/user_id/type',
                                                              params: {
                                                                type: 'string'
                                                              },
                                                              message: 'should be string'
                                                            }];
                                                            return false;
                                                          }
                                                          var valid1 = errors === errs_1;
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        MPUserProfile.errors = [{
          keyword: 'type',
          dataPath: (dataPath || '') + "",
          schemaPath: '#/type',
          params: {
            type: 'object'
          },
          message: 'should be object'
        }];
        return false;
      }
      MPUserProfile.errors = vErrors;
      return errors === 0;
    };
    MPUserProfile.schema = {
      "$id": "MPUserProfile.json",
      "additionalProperties": false,
      "properties": {
        "_id": {
          "$ref": "strings.json#/definitions/_id"
        },
        "_rev": {
          "type": "string"
        },
        "_revisions": {
          "type": "object",
          "properties": {
            "start": {
              "type": "integer"
            },
            "ids": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "additionalItems": false
            }
          }
        },
        "sessionID": {
          "type": "string"
        },
        "createdAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "updatedAt": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "keywordIDs": {
          "type": "array",
          "items": {
            "$ref": "strings.json#/definitions/keywordId"
          }
        },
        "role": {
          "type": "string"
        },
        "isMe": {
          "type": "boolean"
        },
        "category": {
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "name": {
              "type": "string"
            },
            "desc": {
              "type": "string"
            },
            "priority": {
              "type": "number"
            }
          }
        },
        "priority": {
          "type": "number"
        },
        "contribution": {
          "type": "string"
        },
        "placeholderString": {
          "type": "string"
        },
        "appInvitationDate": {
          "$ref": "numbers.json#/definitions/timestamp"
        },
        "addressBookIDs": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "additionalItems": false
        },
        "objectType": {
          "type": "string",
          "enum": ["UserProfile", "MPUserProfile"]
        },
        "bibliographicName": {
          "$ref": "MPBibliographicName.json#"
        },
        "bio": {
          "type": "string"
        },
        "affiliations": {
          "type": "array",
          "items": {
            "$ref": "MPAffiliation.json#"
          },
          "additionalItems": false
        },
        "grants": {
          "type": "array",
          "items": {
            "$ref": "MPGrant.json#"
          },
          "additionalItems": false
        },
        "email": {
          "type": "string"
        },
        "jointContributorGroup": {
          "type": "number"
        },
        "user_id": {
          "type": "string"
        }
      },
      "required": ["_id", "objectType", "user_id"],
      "title": "UserProfile",
      "type": "object"
    };
    MPUserProfile.errors = null;


    function validate(obj) {
      var lookup = {
        Affiliation: MPAffiliation,
        MPAffiliation: MPAffiliation,
        BibliographicDate: MPBibliographicDate,
        MPBibliographicDate: MPBibliographicDate,
        BibliographicName: MPBibliographicName,
        MPBibliographicName: MPBibliographicName,
        BibliographyItem: MPBibliographyItem,
        MPBibliographyItem: MPBibliographyItem,
        FundingBody: MPFundingBody,
        MPFundingBody: MPFundingBody,
        Grant: MPGrant,
        MPGrant: MPGrant,
        Keyword: MPKeyword,
        MPKeyword: MPKeyword,
        Library: MPLibrary,
        MPLibrary: MPLibrary,
        UserProfile: MPUserProfile,
        MPUserProfile: MPUserProfile
      };

      if (!obj) {
        return 'object null or undefined';
      }

      if (!obj.objectType || typeof obj.objectType !== 'string') {
        return 'object missing objectType';
      }

      var validator = lookup[obj.objectType];

      if (!validator) {
        // throw unsupported?
        return 'unsupported objectType: ' + obj.objectType;
      }

      // we could return errors here (validator.errors)
      //
      // i think it's worth considering removing all the error message logic.
      //
      // i think it accounts for around half LOC (search '.errors = [').
      //
      // the clients will most likely want to know, but the sync_gateway doesn't
      // care so we could have some sort of build flag.
      var result = validator(obj);
      if (result) {
        return null;
      } else {
        var err = validator.errors[0];
        var msg = err.message;
        var path = err.dataPath;
        var keyword = err.keyword;
        if (keyword == 'additionalProperties') {
          return msg + " '" + err.params.additionalProperty + "'";
        } else {
          return (path ? path + ': ' : '') + msg;
        }
      }
    }
  }

  // Don't validate deleted documents
  if (!doc._deleted) {
    // check that the update isn't mutating objectType
    if (oldDoc && oldDoc.objectType !== doc.objectType) {
      throw ({
        forbidden: 'objectType cannot be mutated'
      })
    }

    var errorMessage = validate(doc)

    if (errorMessage) {
      throw ({
        forbidden: errorMessage
      })
    }
  }

  function objectTypeMatches(arg) {
    return (doc._deleted ? oldDoc.objectType : doc.objectType) === arg;
  }

  if (objectTypeMatches('Library') || objectTypeMatches('MPLibrary')) {
    channel(doc._id)
  } else {
    channel(doc.library_id)
  }
}

module.exports = {
  syncFn
};
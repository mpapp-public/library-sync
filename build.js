const { join } = require('path');
const { js_beautify } = require('js-beautify');
const { writeFileSync, existsSync, mkdirSync } = require('fs');
const { validatorFn } = require('./pack');

const distDir = join(__dirname, 'dist');

// assert a directory called ./dist exists
if (!existsSync(distDir)){
  mkdirSync(distDir);
}

const code = `\
function syncFn(doc, oldDoc) {
  /* istanbul ignore next */
  {
    ${validatorFn}
  }

  // Don't validate deleted documents
  if (!doc._deleted) {
    // check that the update isn't mutating objectType
    if (oldDoc && oldDoc.objectType !== doc.objectType) {
      throw ({ forbidden: 'objectType cannot be mutated' })
    }

    var errorMessage = validate(doc)

    if (errorMessage) {
      throw ({ forbidden: errorMessage })
    }
  }

  function objectTypeMatches(arg) {
    return (doc._deleted ? oldDoc.objectType : doc.objectType) === arg;
  }

  if (objectTypeMatches('Library') || objectTypeMatches('MPLibrary')) {
    channel(doc._id)
  } else {
    channel(doc.library_id)
  }
}

module.exports = {
  syncFn
};
`;

const fn = js_beautify(code, { indent_size: 2 });

writeFileSync(join(distDir, 'syncFn.js'), fn, 'utf8');
